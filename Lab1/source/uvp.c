#include "uvp.h"

double
absolute(double value) {
  return value * ((value > 0) - (value < 0));
}

double
minimum(double value1,
        double value2,
        double value3) {
  if (value1 < value2) {
    if (value1 < value3)
      return value1;
    else
      return value3;
  } else {
    if (value2 < value3)
      return value2;
    else
      return value3;
  }
}

double
maximum(int      iMax,
        int      jMax,
        double** arrays) {
  double result = absolute(arrays[0][0]);

  for (int j = 0; j < jMax; ++j)
    for (int i = 0; i < iMax; ++i) {
      double absoluteValue = absolute(arrays[i][j]);

      if (result < absoluteValue)
        result = absoluteValue;
    }

  return result;
}

void
calculate_fg(double   Re,
             double   GX,
             double   GY,
             double   alpha,
             double   dt,
             double   dx,
             double   dy,
             int      imax,
             int      jmax,
             double** U,
             double** V,
             double** F,
             double** G) {
  double dXInverse       = 1 / dx;
  double dXInverseFourth = dXInverse / 4;
  double dYInverse       = 1 / dy;
  double dYInverseFourth = dYInverse / 4;
  double reInverse       = 1 / Re;

  for (int j = 1; j <= jmax; ++j) {
    double uNextXSum  = U[0][j] + U[1][j];
    double uNextXDiff = U[0][j] - U[1][j];
    double vNextXSum  = V[0][j] + V[1][j];
    double vNextXDiff = V[0][j] - V[1][j];

    for (int i = 1; i <= imax; ++i) {
      double vPreviovsXSum = vNextXSum;
      vNextXSum = V[i][j] + V[i + 1][j];
      double uNextYSum = U[i][j] + U[i][j + 1];

      // F
      if (i != imax) {
        double uPreviousXSum = uNextXSum;
        uNextXSum = U[i][j] + U[i + 1][j];
        double uPreviousXDiff = -uNextXDiff;
        uNextXDiff = U[i][j] - U[i + 1][j];
        double uNextYDiff         = U[i][j] - U[i][j + 1];
        double uPreviousYSum      = U[i][j] + U[i][j - 1];
        double uPreviousYDiff     = U[i][j] - U[i][j - 1];
        double vPreviousYNextXSum = V[i][j - 1] + V[i + 1][j - 1];

        double uSquarePartialX = dXInverseFourth * (
          uNextXSum * uNextXSum - uPreviousXSum * uPreviousXSum +
          alpha * (absolute(uNextXSum) * uNextXDiff +
                   absolute(uPreviousXSum) * uPreviousXDiff));

        double uVPartialY = dYInverseFourth * (
          vNextXSum * uNextYSum - vPreviousYNextXSum * uPreviousYSum +
          alpha * (absolute(vNextXSum) * uNextYDiff +
                   absolute(vPreviousYNextXSum) *
                   uPreviousYDiff));

        double uSecondPartialX = dXInverse * dXInverse * (
          -uNextXDiff - uPreviousXDiff);

        double uSecondPartialY = dYInverse * dYInverse * (
          -uNextYDiff - uPreviousYDiff);

        F[i][j] = U[i][j] + dt * (
          reInverse * (uSecondPartialX + uSecondPartialY)  -
          uSquarePartialX - uVPartialY + GX);
      }

      // G
      if (j != jmax) {
        double vPreviousXDiff = -vNextXDiff;
        vNextXDiff = V[i][j] - V[i + 1][j];
        double vNextYSum          = V[i][j] + V[i][j + 1];
        double vNextYDiff         = V[i][j] - V[i][j + 1];
        double vPreviousYSum      = V[i][j] + V[i][j - 1];
        double vPreviousYDiff     = V[i][j] - V[i][j - 1];
        double uPreviousXNextYSum = U[i - 1][j] + U[i - 1][j + 1];

        double uVPartialX = dXInverseFourth * (
          uNextYSum * vNextXSum - uPreviousXNextYSum * vPreviovsXSum +
          alpha * (absolute(uNextYSum) * vNextXDiff +
                   absolute(uPreviousXNextYSum) *
                   vPreviousXDiff));

        double vSquarePartialY = dYInverseFourth * (
          vNextYSum * vNextYSum - vPreviousYSum * vPreviousYSum +
          alpha * (absolute(vNextYSum) * vNextYDiff +
                   absolute(vPreviousYSum) * vPreviousYDiff));

        double vSecondPartialX = dXInverse * dXInverse * (
          -vNextXDiff - vPreviousXDiff);

        double vSecondPartialY = dYInverse * dYInverse * (
          -vNextYDiff - vPreviousYDiff);

        G[i][j] = V[i][j] + dt * (
          reInverse * (vSecondPartialX + vSecondPartialY) -
          uVPartialX - vSquarePartialY + GY);
      }
    }
  }

  // Boundary conditions
  for (int j = 1; j <= jmax; ++j) {
    F[0][j]    = U[0][j];
    F[imax][j] = U[imax][j];
  }

  for (int i = 1; i <= imax; ++i) {
    G[i][0]    = V[i][0];
    G[i][jmax] = V[i][jmax];
  }
}

void
calculate_rs(double   dt,
             double   dx,
             double   dy,
             int      imax,
             int      jmax,
             double** F,
             double** G,
             double** RS) {
  double dTInverse = 1 / dt;
  double dXInverse = 1 / dx;
  double dYInverse = 1 / dy;

  for (int j = 1; j <= jmax; ++j)
    for (int i = 1; i <= imax; ++i)
      RS[i][j] = dTInverse * (
        dXInverse * (F[i][j] - F[i - 1][j]) +
        dYInverse * (G[i][j] - G[i][j - 1]));

}

void
calculate_dt(double   Re,
             double   tau,
             double*  dt,
             double   dx,
             double   dy,
             int      imax,
             int      jmax,
             double** U,
             double** V) {
  if (tau <= 0)
    return;

  int    iMax     = imax + 2;
  int    jMax     = jmax + 2;
  double dXSquare = dx * dx;
  double dYSquare = dy * dy;
  double dt1      = (Re / 2) * ((dXSquare * dYSquare) / (dXSquare + dYSquare));
  double uMax     = maximum(iMax, jMax, U);
  double dt2      = (uMax > 0) ? (dx / uMax) : dt1;
  double vMax     = maximum(iMax, jMax, V);
  double dt3      = (vMax > 0) ? dy / vMax : dt1;
  *dt = tau * minimum(dt1, dt2, dt3);
}

void
calculate_uv(double   dt,
             double   dx,
             double   dy,
             int      imax,
             int      jmax,
             double** U,
             double** V,
             double** F,
             double** G,
             double** P) {
  double dTdXQuotient = dt / dx;
  double dTdYQuotient = dt / dy;

  for (int j = 1; j <= jmax; ++j)
    for (int i = 1; i <= imax; ++i) {
      if (i != imax)
        U[i][j] = F[i][j] - dTdXQuotient * (P[i + 1][j] - P[i][j]);

      if (j != jmax)
        V[i][j] = G[i][j] - dTdYQuotient * (P[i][j + 1] - P[i][j]);
    }

}
