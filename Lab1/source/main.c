#include "boundary_val.h"
#include "helper.h"
#include "init.h"
#include "sor.h"
#include "uvp.h"
#include "visual.h"
#include <stdio.h>

/**
 * The main operation reads the configuration file, initializes the scenario
 * and
 * contains the main loop. So here are the individual steps of the algorithm:
 *
 * - read the program configuration file using read_parameters()
 * - set up the matrices (arrays) needed using the matrix() command
 * - create the initial setup init_uvp(), init_flag(), output_uvp()
 * - perform the main loop
 * - trailer: destroy memory allocated and do some statistics
 *
 * The layout of the grid is decribed by the first figure below, the
 * enumeration
 * of the whole grid is given by the second figure. All the unknowns corresond
 * to a two dimensional degree of freedom layout, so they are not stored in
 * arrays, but in a matrix.
 *
 * @image html grid.jpg
 *
 * @image html whole-grid.jpg
 *
 * Within the main loop the following big steps are done (for some of the
 * operations a definition is defined already within uvp.h):
 *
 * - calculate_dt() Determine the maximal time step size.
 * - boundaryvalues() Set the boundary values for the next time step.
 * - calculate_fg() Determine the values of F and G (diffusion and confection).
 *   This is the right hand side of the pressure equation and used later on for
 *   the time step transition.
 * - calculate_rs()
 * - Iterate the pressure poisson equation until the residual becomes smaller
 *   than eps or the maximal number of iterations is performed. Within the
 *   iteration loop the operation sor() is used.
 * - calculate_uv() Calculate the velocity at the next time step.
 */
int
main(int    argn,
     char** args) {
  double Re;
  double UI;
  double VI;
  double PI;
  double GX;
  double GY;
  double t_end;
  double xlength;
  double ylength;
  double dt;
  double dx;
  double dy;
  int    imax;
  int    jmax;
  double alpha;
  double omg;
  double tau;
  int    itermax;
  double eps;
  double dt_value;

  read_parameters("Cavity.dat",
                  &Re,
                  &UI,
                  &VI,
                  &PI,
                  &GX,
                  &GY,
                  &t_end,
                  &xlength,
                  &ylength,
                  &dt,
                  &dx,
                  &dy,
                  &imax,
                  &jmax,
                  &alpha,
                  &omg,
                  &tau,
                  &itermax,
                  &eps,
                  &dt_value);
  int iMax = imax + 2;
  int jMax = jmax + 2;

  double** U  = matrix(0, imax + 1, 0, jmax + 1);
  double** V  = matrix(0, imax + 1, 0, jmax + 1);
  double** P  = matrix(0, imax + 1, 0, jmax + 1);
  double** F  = matrix(0, imax + 1, 0, jmax + 1);
  double** G  = matrix(0, imax + 1, 0, jmax + 1);
  double** RS = matrix(1, imax, 1, jmax);

  double t = 0;
  int    n = 0;

  int sorIterationNumber  = 0;
  int stabilityCondition1 = 1;
  int stabilityCondition2 = 1;
  int stabilityCondition3 = 1;

  init_uvp(UI,
           VI,
           PI,
           imax,
           jmax,
           U,
           V,
           P);

  double uMax = maximum(iMax, jMax, U);
  double vMax = maximum(iMax, jMax, V);
  double maxU = uMax;
  double maxV = vMax;

  while (t < t_end) {
    calculate_dt(Re,
                 tau,
                 &dt,
                 dx,
                 dy,
                 imax,
                 jmax,
                 U,
                 V);

    if (stabilityCondition2 == 1)
      stabilityCondition2 = (uMax > 0) ? (dt < (dx / uMax)) : 1;

    if (stabilityCondition3 == 1)
      stabilityCondition3 = (vMax > 0) ? (dt < (dy / vMax)) : 1;

    boundaryvalues(imax,
                   jmax,
                   U,
                   V);

    calculate_fg(Re,
                 GX,
                 GY,
                 alpha,
                 dt,
                 dx,
                 dy,
                 imax,
                 jmax,
                 U,
                 V,
                 F,
                 G);

    calculate_rs(dt,
                 dx,
                 dy,
                 imax,
                 jmax,
                 F,
                 G,
                 RS);

    double res = eps + 1;
    int    it  = 0;

    while ((it < itermax) && (res > eps)) {
      sor(omg,
          dx,
          dy,
          imax,
          jmax,
          P,
          RS,
          &res);
      ++it;
    }

    sorIterationNumber += it;

    calculate_uv(dt,
                 dx,
                 dy,
                 imax,
                 jmax,
                 U,
                 V,
                 F,
                 G,
                 P);

    ++n;
    t += dt;
    write_vtkFile("Cavity",
                  n,
                  xlength,
                  ylength,
                  imax,
                  jmax,
                  dx,
                  dy,
                  U,
                  V,
                  P);

    uMax = maximum(iMax, jMax, U);

    if (uMax > maxU)
      maxU = uMax;

    vMax = maximum(iMax, jMax, V);

    if (vMax > maxV)
      maxV = vMax;
  }

  printf("Average time step is %f\n", t / n);
  printf("Number of steps is %u\n", n);
  printf("Average Number of Iterations of SOR solver: %f\n",
         (double)(sorIterationNumber / n));
  stabilityCondition1 =
    (dt < ((Re / 2) * ((dx * dx * dy * dy) / (dx * dx + dy * dy))));
  printf("Stability Condition 1 is %u (1 - true, 0 - false)\n",
         stabilityCondition1);
  printf("Stability Condition 2 is %u (1 - true, 0 - false)\n",
         stabilityCondition2);
  printf("Stability Condition 3 is %u (1 - true, 0 - false)\n",
         stabilityCondition3);
  printf("Maximum u velocity is %f\n",
         maxU);
  printf("Maximum v velocity is %f\n",
         maxV);
  printf("U[imax/2][7*jmax/8] = %f\n",
         U[imax / 2][7 * jmax / 8]);

  free_matrix(U,     0, imax + 1, 0, jmax + 1);
  free_matrix(V,     0, imax + 1, 0, jmax + 1);
  free_matrix(P,     0, imax + 1, 0, jmax + 1);
  free_matrix(F,     0, imax + 1, 0, jmax + 1);
  free_matrix(G,     0, imax + 1, 0, jmax + 1);
  free_matrix(RS,    1, imax,     1, jmax);
}
