#include "streaming.h"

#include "LBDefinitions.h"
#include "computeCellValues.h"

void
doStreaming(double* collideField,
            double* streamField,
            int*    flagField,
            int     xlength) {
  for (int x = 0; x < xlength + 2; ++x)
    for (int y = 0; y < xlength + 2; ++y)
      for (int z = 0; z < xlength + 2; ++z) {
        if (flagField[FLAG_INDEX3(x, y, z, xlength + 2)] != FLUID)
          continue;

        double* streamCell = streamField + CELL_INDEX3(x, y, z, xlength + 2);

        for (int i = 0; i < Q; ++i) {
          int const* const c = LATTICEVELOCITIES[i];

          double* collideCell = collideField + CELL_INDEX3(x - c[0],
                                                           y - c[1],
                                                           z - c[2],
                                                           xlength + 2);

          streamCell[i] = collideCell[i];
        }
      }

}
