#include "LBDefinitions.h"
#include "boundary.h"
#include "collision.h"
#include "initLB.h"
#include "streaming.h"
#include "visualLB.h"

int
main(int argc, char* argv[]) {
  int    xlength;
  double tau;
  double wallVelocity[3];

  int timesteps;
  int timestepsPerPlotting;
  int t;

  char const* fileName = "output/Cavity";

  if (readParameters(&xlength,
                     &tau,
                     wallVelocity,
                     &timesteps,
                     &timestepsPerPlotting,
                     argc,
                     argv))
    return 1;

  size_t cellFieldSize = CELL_FIELD_SIZE3(xlength + 2);
  size_t flagFieldSize = FLAG_FIELD_SIZE3(xlength + 2);

  double* collideField = (double*)malloc(cellFieldSize * sizeof(double));
  double* streamField  = (double*)malloc(cellFieldSize * sizeof(double));
  int*    flagField    = (int*)malloc(flagFieldSize * sizeof(int));

  initialiseFields(collideField, streamField, flagField, xlength);

  for (t = 0; t < timesteps; ++t) {
    double* swap = 0;

    doStreaming(collideField, streamField, flagField, xlength);

    swap         = collideField;
    collideField = streamField;
    streamField  = swap;

    doCollision(collideField, flagField, &tau, xlength);
    treatBoundary(collideField, flagField, wallVelocity, xlength);

    if (t % timestepsPerPlotting == 0)
      writeVtkOutput(collideField, flagField, fileName, t, xlength);
  }
}
