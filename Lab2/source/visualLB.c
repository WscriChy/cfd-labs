#include "visualLB.h"

#include "LBDefinitions.h"
#include "computeCellValues.h"
#include "helper.h"

#include <stdio.h>

void
writeVtkHeader3(FILE* file);

void
writeVtkPoints3(FILE* file, int xlength);

void
writeVtkVelocity3(FILE*               file,
                  int                 xlength,
                  double const* const collideField,
                  int const* const    flagField);

void
writeVtkDensity3(FILE*               file,
                 int                 xlength,
                 double const* const collideField,
                 int const* const    flagField);

void
writeVtkOutput(double const* const collideField,
               int const* const    flagField,
               char const*         fileName,
               int                 t,
               int                 xlength) {
  char timestepFileName[256];

  sprintf(timestepFileName, "%s.%i.vtk", fileName, t);

  FILE* file = fopen(timestepFileName, "w");

  if (file == 0) {
    char message[18 + 256];

    sprintf(message, "Failed to open '%s'.", timestepFileName);
    ERROR(message);

    exit(1);
  }

  writeVtkHeader3(file);
  writeVtkPoints3(file, xlength);
  writeVtkVelocity3(file, xlength, collideField, flagField);
  writeVtkDensity3(file, xlength, collideField, flagField);

  if (fclose(file)) {
    char message[19 + 256];

    sprintf(message, "Failed to close '%s'.", timestepFileName);
    ERROR(message);

    exit(1);
  }
}

void
writeVtkHeader3(FILE* file) {
  fprintf(file, "# vtk DataFile Version 2.0\n");
  fprintf(file, "The Lattice-Boltzmann Method\n");
  fprintf(file, "ASCII\n");
  fprintf(file, "\n");
}

void
writeVtkPoints3(FILE* file, int xlength) {
  fprintf(file, "DATASET STRUCTURED_GRID\n");
  fprintf(file, "DIMENSIONS %i %i %i\n", xlength, xlength, xlength);
  fprintf(file, "POINTS %i double\n", xlength * xlength * xlength);
  fprintf(file, "\n");

  double originX = 0;
  double originY = 0;
  double originZ = 0;

  for (int x = 0; x < xlength; ++x)
    for (int y = 0; y < xlength; ++y)
      for (int z = 0; z < xlength; ++z)
        fprintf(file, "%f %f %f\n", originX + x, originY + y, originZ + z);

  fprintf(file, "\n");
}

void
writeVtkVelocity3(FILE*               file,
                  int                 xlength,
                  double const* const collideField,
                  int const* const    flagField) {
  UNUSED(flagField);

  fprintf(file, "POINT_DATA %i\n", xlength * xlength * xlength);
  fprintf(file, "VECTORS velocity double\n");
  fprintf(file, "\n");

  for (int x = 1; x < xlength + 1; ++x)
    for (int y = 1; y < xlength + 1; ++y)
      for (int z = 1; z < xlength + 1; ++z) {
        double const* currentCell = collideField +
                                    CELL_INDEX3(x, y, z, xlength + 2);

        double density;
        double velocity[3];

        computeDensity(currentCell, &density);
        computeVelocity(currentCell, &density, velocity);

        fprintf(file, "%f %f %f\n", velocity[0], velocity[1], velocity[2]);
      }

  fprintf(file, "\n");
}

void
writeVtkDensity3(FILE*               file,
                 int                 xlength,
                 double const* const collideField,
                 int const* const    flagField) {
  UNUSED(flagField);

  fprintf(file, "CELL_DATA %i\n",
          (xlength - 1) * (xlength - 1) * (xlength - 1));
  fprintf(file, "SCALARS density double 1\n");
  fprintf(file, "LOOKUP_TABLE default\n");
  fprintf(file, "\n");

  for (int x = 1; x < xlength; ++x)
    for (int y = 1; y < xlength; ++y)
      for (int z = 1; z < xlength; ++z) {
        double const* currentCell = collideField +
                                    CELL_INDEX3(x, y, z, xlength + 2);

        double density;

        computeDensity(currentCell, &density);

        fprintf(file, "%f\n", density);
      }

  fprintf(file, "\n");
}
