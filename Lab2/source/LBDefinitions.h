#ifndef _LBDEFINITIONS_H_
#define _LBDEFINITIONS_H_

#include <math.h>

// That was sort of wierd decision to put `static` variables into the header
// file as it results in unnecessary duplication of all these variables across
// all the transaltion units which include this header file. What you really
// wanted to do was to declare these variables as `extern` in header file, and
// define them in some transaltion unit (perhaps `LBDefinitions.c`). To
// conclude, 1 header declares the variable as `extern`, 1 transaltion unit
// defines the variable (and can reference it too), all other translation units
// can only reference the variable.

#define Q 19

static int const LATTICEVELOCITIES[Q][3] = { { 0,  -1,  -1   },
                                             { -1, 0,   -1   },

                                             { 0,  0,   -1   },

                                             { 1,  0,   -1   },
                                             { 0,  1,   -1   },
                                             { -1, -1,  0    },

                                             { 0,  -1,  0    },

                                             { 1,  -1,  0    },

                                             { -1, 0,   0    },

                                             { 0,  0,   0    },

                                             { 1,  0,   0    },

                                             { -1, 1,   0    },

                                             { 0,  1,   0    },

                                             { 1,  1,   0    },
                                             { 0,  -1,  1    },
                                             { -1, 0,   1    },

                                             { 0,  0,   1    },

                                             { 1,  0,   1    },
                                             { 0,  1,   1    } };

static double const LATTICEWEIGHTS[Q] = { 1.0 / 36.0,
                                          1.0 / 36.0,

                                          2.0 / 36.0,

                                          1.0 / 36.0,
                                          1.0 / 36.0,
                                          1.0 / 36.0,

                                          2.0 / 36.0,

                                          1.0 / 36.0,

                                          2.0 / 36.0,

                                          12.0 / 36.0,

                                          2.0 / 36.0,

                                          1.0 / 36.0,

                                          2.0 / 36.0,

                                          1.0 / 36.0,
                                          1.0 / 36.0,
                                          1.0 / 36.0,

                                          2.0 / 36.0,

                                          1.0 / 36.0,
                                          1.0 / 36.0 };

static double const C_S =
  1.0 / 1.732050807568877193176604123436845839023590087890625;

#define FLUID       0
#define NO_SLIP     1
#define MOVING_WALL 2

#define FLAG_FIELD_SIZE3(length) \
  ((length) * (length) * (length))

#define CELL_FIELD_SIZE3(length) \
  (Q * FLAG_FIELD_SIZE3(length))

#define FLAG_INDEX3(x, y, z, length) \
  ((x) + (y) * (length) + (z) * (length) * (length))

#define CELL_INDEX3(x, y, z, length) \
  (Q * FLAG_INDEX3(x, y, z, length))

#define DOT3(x, y) \
  (x[0] * y[0] + x[1] * y[1] + x[2] * y[2])

#define C_S_2 \
  (C_S * C_S)

#define C_S_4 \
  (C_S_2 * C_S_2)

#endif
