#include "initLB.h"

#include "LBDefinitions.h"
#include "computeCellValues.h"

#include <string.h>

int
readParameters(int*    xlength,
               double* tau,
               double* wallVelocity,
               int*    timesteps,
               int*    timestepsPerPlotting,
               int     argc,
               char*   argv[]) {
  if (argc != 2)
    return 1;

  char const* fileName = argv[1];

  READ_INT(fileName, *xlength);

  READ_DOUBLE(fileName, *tau);

  read_double(fileName, "wallVelocityX", &wallVelocity[0]);
  read_double(fileName, "wallVelocityY", &wallVelocity[1]);
  read_double(fileName, "wallVelocityZ", &wallVelocity[2]);

  READ_INT(fileName, *timesteps);
  READ_INT(fileName, *timestepsPerPlotting);

  return 0;
}

void
initialiseFields(double* collideField,
                 double* streamField,
                 int*    flagField,
                 int     xlength) {
#if 0
  double density     = 1;
  double velocity[3] = { 0 };
  double feq[Q];

  computeFeq(&density, velocity, feq);
#else
  double const* feq = LATTICEWEIGHTS;
#endif

  for (int x = 0; x < xlength + 2; ++x)
    for (int y = 0; y < xlength + 2; ++y)
      for (int z = 0; z < xlength + 2; ++z) {
        int cellIndex = CELL_INDEX3(x, y, z, xlength + 2);

        memcpy(collideField + cellIndex, feq, Q * sizeof(double));
        memcpy(streamField  + cellIndex, feq, Q * sizeof(double));
      }



  for (int x = 1; x < xlength + 1; ++x)
    for (int y = 1; y < xlength + 1; ++y)
      for (int z = 1; z < xlength + 1; ++z)
        flagField[FLAG_INDEX3(x, y, z, xlength + 2)] = FLUID;



  for (int x = 0; x < xlength + 2; ++x)
    for (int y = 0; y < xlength + 2; ++y) {
      // Forward Wall
      flagField[FLAG_INDEX3(x, y, 0,           xlength + 2)] = NO_SLIP;

      // Backward Wall
      flagField[FLAG_INDEX3(x, y, xlength + 1, xlength + 2)] = NO_SLIP;
    }



  for (int y = 0; y < xlength + 2; ++y)
    for (int z = 0; z < xlength + 2; ++z) {
      // Left Wall
      flagField[FLAG_INDEX3(0,           y, z, xlength + 2)] = NO_SLIP;

      // Right Wall
      flagField[FLAG_INDEX3(xlength + 1, y, z, xlength + 2)] = NO_SLIP;
    }



  for (int x = 0; x < xlength + 2; ++x)
    for (int z = 0; z < xlength + 2; ++z) {
      // Bottom Wall
      flagField[FLAG_INDEX3(x, 0,           z, xlength + 2)] = NO_SLIP;

      // Top Wall
      flagField[FLAG_INDEX3(x, xlength + 1, z, xlength + 2)] = MOVING_WALL;
    }

}
