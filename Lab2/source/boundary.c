#include "boundary.h"

#include "LBDefinitions.h"
#include "computeCellValues.h"

#include <assert.h>
#include <stdio.h>

int
isFluidCellIndex3(int* flagField, int x, int y, int z, int xlength) {
  return x >= 0 && x <= xlength + 1 &&
         y >= 0 && y <= xlength + 1 &&
         z >= 0 && z <= xlength + 1 &&
         flagField[FLAG_INDEX3(x, y, z, xlength + 2)] == FLUID;
}

void
treatBoundary(double*             collideField,
              int*                flagField,
              double const* const wallVelocity,
              int                 xlength) {
  for (int x = 0; x < xlength + 2; ++x)
    for (int y = 0; y < xlength + 2; ++y)
      for (int z = 0; z < xlength + 2; ++z) {
        int flagIndex = FLAG_INDEX3(x, y, z, xlength + 2);

        if (flagField[flagIndex] == FLUID)
          continue;

        double* boundaryCell = collideField +
                               CELL_INDEX3(x, y, z, xlength + 2);

        for (int i = 0; i < Q; ++i) {
          int const* const c = LATTICEVELOCITIES[i];

          if (!isFluidCellIndex3(flagField,
                                 x + c[0],
                                 y + c[1],
                                 z + c[2],
                                 xlength))
            continue;

          double* fluidCell = collideField + CELL_INDEX3(x + c[0],
                                                         y + c[1],
                                                         z + c[2],
                                                         xlength + 2);

          switch (flagField[flagIndex]) {
          case NO_SLIP:
            boundaryCell[i] = fluidCell[Q - 1 - i];

            break;

          case MOVING_WALL: {
            double density;

            computeDensity(fluidCell, &density);

            boundaryCell[i] = fluidCell[Q - 1 - i] +
                              2 * LATTICEWEIGHTS[i] * (density) *
                              DOT3(c, wallVelocity) / C_S_2;

            break;
          }

          default:
            printf("This can never happen!");

            return;
          }

          assert(boundaryCell[i] >= 0);
        }
      }

}
