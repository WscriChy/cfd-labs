Arbitrary Geometries with Navier-Stokes Equations
=

## Building/Installation

    make

## Run

    cd install
    sim <option>

    <option> could be
           '1' - The Karman Vortex Street;
           '2' - Plane shear flow;
           '3' - Flow over a step.

## Notes

There are three data files for each scenario.
These files are not the recommended ones in the worksheet.
We found out that these parameters suit better for the scenarios.
If you would like to use the recommended parameters, please, just replace
these files with files from `install/default` directory.
