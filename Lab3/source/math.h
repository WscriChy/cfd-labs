double
absolute(double value);

double
minimum(double value1,
        double value2,
        double value3);

double
maximum(int      iMax,
        int      jMax,
        double** arrays);
