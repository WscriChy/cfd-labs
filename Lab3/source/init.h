#ifndef __INIT_H_
#define __INIT_H_

int
read_parameters(const char* szFileName, /* name of the file */
                double*     Re,         /* reynolds number */
                double*     UI,         /* velocity x-direction */
                double*     VI,         /* velocity y-direction */
                double*     PI,         /* pressure */
                double*     GX,         /* gravitation x-direction */
                double*     GY,         /* gravitation y-direction */
                double*     t_end,      /* end time */
                double*     xlength,    /* length of the domain x-dir.*/
                double*     ylength,    /* length of the domain y-dir.*/
                double*     dt,         /* time step */
                double*     dx,         /* length of a cell x-dir. */
                double*     dy,         /* length of a cell y-dir. */
                int*        imax,       /* number of cells x-direction*/
                int*        jmax,       /* number of cells y-direction*/
                double*     alpha,      /* uppwind differencing factor*/
                double*     omg,        /* relaxation factor */
                double*     tau,        /* safety factor for time step*/
                int*        itermax,    /* max. number of iterations */
                double*     dP,         /* pressure difference */
                int*        wl,         /* left boundary condition */
                int*        wr,         /* right boundary condition */
                int*        wt,         /* top boundary condition */
                int*        wb,         /* bottom boundary condition */
                /* for pressure per time step */
                double*     eps,        /* accuracy bound for pressure*/
                double*     dt_value);  /* time for output */

void
init_uvp(char*    problem,
         double   UI,
         double   VI,
         double   PI,
         int      imax,
         int      jmax,
         double   dP,
         double** U,
         double** V,
         double** P,
         int**    Flag);

void
init_flag(char* problem,
          int   imax,
          int   jmax,
          int** Flag);

#endif
