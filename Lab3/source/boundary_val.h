#ifndef __RANDWERTE_H__
#define __RANDWERTE_H__

void
setFgBoundary(int      imax,
              int      jmax,
              double** U,
              double** V,
              double** F,
              double** G,
              int**    Flag);

void
setPressureBoundary(int      imax,
                    int      jmax,
                    double   dP,
                    double** P,
                    int**    Flag);

void
boundaryvalues(int      imax,
               int      jmax,
               int      wl,
               int      wr,
               int      wt,
               int      wb,
               double** U,
               double** V,
               int**    Flag);

void
spec_boundary_val(char*    problem,
                  int      imax,
                  int      jmax,
                  double** U,
                  double** V);

#endif
