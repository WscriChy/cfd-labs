#include "boundary_val.h"
#include "math.h"
#include "scene.h"
#include "uvp.h"

void
calculate_fg(double   Re,
             double   GX,
             double   GY,
             double   alpha,
             double   dt,
             double   dx,
             double   dy,
             int      imax,
             int      jmax,
             double** U,
             double** V,
             double** F,
             double** G,
             int**    Flag) {
  double dXInverse       = 1 / dx;
  double dXInverseFourth = dXInverse / 4;
  double dYInverse       = 1 / dy;
  double dYInverseFourth = dYInverse / 4;
  double reInverse       = 1 / Re;

  for (int j = 1; j <= jmax; ++j)

    for (int i = 1; i <= imax; ++i) {
      // F
      if (isFluidRightAdjacentCell(Flag[i][j])) {
        double vNextXSum          = V[i][j] + V[i + 1][j];
        double uNextYSum          = U[i][j] + U[i][j + 1];
        double uPreviousXSum      = U[i - 1][j] + U[i][j];
        double uNextXSum          = U[i][j] + U[i + 1][j];
        double uPreviousXDiff     = U[i][j] - U[i - 1][j];
        double uNextXDiff         = U[i][j] - U[i + 1][j];
        double uNextYDiff         = U[i][j] - U[i][j + 1];
        double uPreviousYSum      = U[i][j] + U[i][j - 1];
        double uPreviousYDiff     = U[i][j] - U[i][j - 1];
        double vPreviousYNextXSum = V[i][j - 1] + V[i + 1][j - 1];

        double uSquarePartialX = dXInverseFourth * (
          uNextXSum * uNextXSum - uPreviousXSum * uPreviousXSum +
          alpha * (absolute(uNextXSum) * uNextXDiff +
                   absolute(uPreviousXSum) * uPreviousXDiff));

        double uVPartialY = dYInverseFourth * (
          vNextXSum * uNextYSum - vPreviousYNextXSum * uPreviousYSum +
          alpha * (absolute(vNextXSum) * uNextYDiff +
                   absolute(vPreviousYNextXSum) *
                   uPreviousYDiff));

        double uSecondPartialX = dXInverse * dXInverse * (
          -uNextXDiff - uPreviousXDiff);

        double uSecondPartialY = dYInverse * dYInverse * (
          -uNextYDiff - uPreviousYDiff);

        F[i][j] = U[i][j] + dt * (
          reInverse * (uSecondPartialX + uSecondPartialY)  -
          uSquarePartialX - uVPartialY + GX);
      }

      // G
      if (isFluidTopAdjacentCell(Flag[i][j])) {
        double vPreviovsXSum      = V[i - 1][j] + V[i][j];
        double vNextXSum          = V[i][j] + V[i + 1][j];
        double uNextYSum          = U[i][j] + U[i][j + 1];
        double vPreviousXDiff     = V[i][j] - V[i - 1][j];
        double vNextXDiff         = V[i][j] - V[i + 1][j];
        double vNextYSum          = V[i][j] + V[i][j + 1];
        double vNextYDiff         = V[i][j] - V[i][j + 1];
        double vPreviousYSum      = V[i][j] + V[i][j - 1];
        double vPreviousYDiff     = V[i][j] - V[i][j - 1];
        double uPreviousXNextYSum = U[i - 1][j] + U[i - 1][j + 1];

        double uVPartialX = dXInverseFourth * (
          uNextYSum * vNextXSum - uPreviousXNextYSum * vPreviovsXSum +
          alpha * (absolute(uNextYSum) * vNextXDiff +
                   absolute(uPreviousXNextYSum) *
                   vPreviousXDiff));

        double vSquarePartialY = dYInverseFourth * (
          vNextYSum * vNextYSum - vPreviousYSum * vPreviousYSum +
          alpha * (absolute(vNextYSum) * vNextYDiff +
                   absolute(vPreviousYSum) * vPreviousYDiff));

        double vSecondPartialX = dXInverse * dXInverse * (
          -vNextXDiff - vPreviousXDiff);

        double vSecondPartialY = dYInverse * dYInverse * (
          -vNextYDiff - vPreviousYDiff);

        G[i][j] = V[i][j] + dt * (
          reInverse * (vSecondPartialX + vSecondPartialY) -
          uVPartialX - vSquarePartialY + GY);
      }
    }

  setFgBoundary(imax,
                jmax,
                U,
                V,
                F,
                G,
                Flag);

  // Boundary conditions
  for (int j = 1; j <= jmax; ++j) {
    F[0][j]    = U[0][j];
    F[imax][j] = U[imax][j];
  }

  for (int i = 1; i <= imax; ++i) {
    G[i][0]    = V[i][0];
    G[i][jmax] = V[i][jmax];
  }
}

void
calculate_rs(double   dt,
             double   dx,
             double   dy,
             int      imax,
             int      jmax,
             double** F,
             double** G,
             double** RS,
             int**    Flag) {
  double dTInverse = 1 / dt;
  double dXInverse = 1 / dx;
  double dYInverse = 1 / dy;

  for (int j = 1; j <= jmax; ++j)
    for (int i = 1; i <= imax; ++i)
      if (isFluidCell(Flag[i][j]))
        RS[i][j] = dTInverse * (
          dXInverse * (F[i][j] - F[i - 1][j]) +
          dYInverse * (G[i][j] - G[i][j - 1]));

}

void
calculate_dt(double   Re,
             double   tau,
             double*  dt,
             double   dx,
             double   dy,
             int      imax,
             int      jmax,
             double** U,
             double** V) {
  if (tau <= 0)
    return;

  int    iMax     = imax + 2;
  int    jMax     = jmax + 2;
  double dXSquare = dx * dx;
  double dYSquare = dy * dy;
  double dt1      = (Re / 2) * ((dXSquare * dYSquare) / (dXSquare + dYSquare));
  double uMax     = maximum(iMax, jMax, U);
  double dt2      = (uMax > 0) ? (dx / uMax) : dt1;
  double vMax     = maximum(iMax, jMax, V);
  double dt3      = (vMax > 0) ? dy / vMax : dt1;
  *dt = tau * minimum(dt1, dt2, dt3);
}

void
calculate_uv(double   dt,
             double   dx,
             double   dy,
             int      imax,
             int      jmax,
             double** U,
             double** V,
             double** F,
             double** G,
             double** P,
             int**    Flag) {
  double dTdXQuotient = dt / dx;
  double dTdYQuotient = dt / dy;

  for (int j = 1; j <= jmax; ++j)
    for (int i = 1; i <= imax; ++i) {
      if (isFluidRightAdjacentCell(Flag[i][j]))
        U[i][j] = F[i][j] - dTdXQuotient * (P[i + 1][j] - P[i][j]);

      if (isFluidTopAdjacentCell(Flag[i][j]))
        V[i][j] = G[i][j] - dTdYQuotient * (P[i][j + 1] - P[i][j]);
    }

}
