#include "boundary_val.h"
#include "helper.h"
#include "init.h"
#include "math.h"
#include "scene.h"
#include "sor.h"
#include "uvp.h"
#include "visual.h"
#include <stdio.h>

int
main(int    argn,
     char** args) {
  char* problem;
  char* dataFile;
  char* vtkFilesName;

  if (argn < 2) {
    printf("Please, pass the name of the problem you want to simulate\n");
    printf("Allowed names are:\n"
           "'1' - The Karman Vortex Street;\n"
           "'2' - Plane shear flow;\n"
           "'3' - Flow over a step.\n");
    exit(1);
  } else {
    problem = args[1];
    int problemSize  = strlen(problem);
    int dataFileSize = problemSize + 5;
    dataFile = malloc(sizeof(char) * dataFileSize);
    sprintf(dataFile, "%s.dat", problem);
    int vtkFilesNameSize = 2 * problemSize + 2;
    vtkFilesName = malloc(sizeof(char) * vtkFilesNameSize);
    sprintf(vtkFilesName, "%s/%s", problem, problem);
  }

  double Re;
  double UI;
  double VI;
  double PI;
  double GX;
  double GY;
  double t_end;
  double xlength;
  double ylength;
  double dt;
  double dx;
  double dy;
  int    imax;
  int    jmax;
  double alpha;
  double omg;
  double tau;
  int    itermax;
  double dP;
  int    wl;
  int    wr;
  int    wt;
  int    wb;
  double eps;
  double dt_value;

  read_parameters(dataFile,
                  &Re,
                  &UI,
                  &VI,
                  &PI,
                  &GX,
                  &GY,
                  &t_end,
                  &xlength,
                  &ylength,
                  &dt,
                  &dx,
                  &dy,
                  &imax,
                  &jmax,
                  &alpha,
                  &omg,
                  &tau,
                  &itermax,
                  &dP,
                  &wl,
                  &wr,
                  &wt,
                  &wb,
                  &eps,
                  &dt_value);
  int iMax = imax + 2;
  int jMax = jmax + 2;

  double** U    = matrix(0, imax + 1, 0, jmax + 1);
  double** V    = matrix(0, imax + 1, 0, jmax + 1);
  double** P    = matrix(0, imax + 1, 0, jmax + 1);
  double** F    = matrix(0, imax + 1, 0, jmax + 1);
  double** G    = matrix(0, imax + 1, 0, jmax + 1);
  double** RS   = matrix(1, imax, 1, jmax);
  int**    Flag = imatrix(0, imax + 1, 0, jmax + 1);

  double t             = 0.0;
  double timer         = 0.0;
  double residual      = 0.0;
  int    residualCount = 0;
  int    n             = 0;

  int sorIterationNumber  = 0;
  int stabilityCondition1 = 1;
  int stabilityCondition2 = 1;
  int stabilityCondition3 = 1;

  init_flag(problem,
            imax,
            jmax,
            Flag);

  init_uvp(problem,
           UI,
           VI,
           PI,
           imax,
           jmax,
           dP,
           U,
           V,
           P,
           Flag);

  double uMax = maximum(iMax, jMax, U);
  double vMax = maximum(iMax, jMax, V);
  double maxU = uMax;
  double maxV = vMax;

  while (t < t_end) {
    calculate_dt(Re,
                 tau,
                 &dt,
                 dx,
                 dy,
                 imax,
                 jmax,
                 U,
                 V);

    if (stabilityCondition2 == 1)
      stabilityCondition2 = (uMax > 0) ? (dt < (dx / uMax)) : 1;

    if (stabilityCondition3 == 1)
      stabilityCondition3 = (vMax > 0) ? (dt < (dy / vMax)) : 1;

    boundaryvalues(imax,
                   jmax,
                   wl,
                   wr,
                   wt,
                   wb,
                   U,
                   V,
                   Flag);

    spec_boundary_val(problem,
                      imax,
                      jmax,
                      U,
                      V);

    calculate_fg(Re,
                 GX,
                 GY,
                 alpha,
                 dt,
                 dx,
                 dy,
                 imax,
                 jmax,
                 U,
                 V,
                 F,
                 G,
                 Flag);

    calculate_rs(dt,
                 dx,
                 dy,
                 imax,
                 jmax,
                 F,
                 G,
                 RS,
                 Flag);

    double res = eps + 1;
    int    it  = 0;

    while ((it < itermax) && (res > eps)) {
      sor(omg,
          dx,
          dy,
          imax,
          jmax,
          P,
          RS,
          dP,
          Flag,
          &res);
      ++it;
    }

    sorIterationNumber += it;

    calculate_uv(dt,
                 dx,
                 dy,
                 imax,
                 jmax,
                 U,
                 V,
                 F,
                 G,
                 P,
                 Flag);

    ++n;
    ++residualCount;
    residual += res;
    t        += dt;
    timer    += dt;

    if (timer >= dt_value) {
      write_vtkFile(vtkFilesName,
                    n,
                    xlength,
                    ylength,
                    imax,
                    jmax,
                    dx,
                    dy,
                    U,
                    V,
                    P);
      printf("SOR residual is %f\n", residual / residualCount);
      residualCount = 0;
      residual      = 0.0;
      timer         = timer - dt_value;
    }

    uMax = maximum(iMax, jMax, U);

    if (uMax > maxU)
      maxU = uMax;

    vMax = maximum(iMax, jMax, V);

    if (vMax > maxV)
      maxV = vMax;
  }

  printf("Average time step is %f\n", t / n);
  printf("Number of steps is %u\n", n);
  printf("Average Number of Iterations of SOR solver: %f\n",
         (double)(sorIterationNumber / n));
  stabilityCondition1 =
    (dt < ((Re / 2) * ((dx * dx * dy * dy) / (dx * dx + dy * dy))));
  printf("Stability Condition 1 is %u (1 - true, 0 - false)\n",
         stabilityCondition1);
  printf("Stability Condition 2 is %u (1 - true, 0 - false)\n",
         stabilityCondition2);
  printf("Stability Condition 3 is %u (1 - true, 0 - false)\n",
         stabilityCondition3);
  printf("Maximum u velocity is %f\n",
         maxU);
  printf("Maximum v velocity is %f\n",
         maxV);
  printf("U[imax/2][7*jmax/8] = %f\n",
         U[imax / 2][7 * jmax / 8]);

  free_matrix(U,     0, imax + 1, 0, jmax + 1);
  free_matrix(V,     0, imax + 1, 0, jmax + 1);
  free_matrix(P,     0, imax + 1, 0, jmax + 1);
  free_matrix(F,     0, imax + 1, 0, jmax + 1);
  free_matrix(G,     0, imax + 1, 0, jmax + 1);
  free_matrix(RS,    1, imax,     1, jmax);
  free_imatrix(Flag,    0, imax + 1,     0, jmax + 1);
  free(dataFile);
  free(vtkFilesName);
}
