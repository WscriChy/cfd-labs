#include "scene.h"

int
isFluidCell(int flag) {
  return (flag & C_F) == C_F;
}

int
isFluidRightAdjacentCell(int flag) {
  return isFluidCell(flag) && ((flag & B_E) == B_E);
}

int
isFluidTopAdjacentCell(int flag) {
  return isFluidCell(flag) && ((flag & B_N) == B_N);
}

int
isInternalBoundaryCell(int flag) {
  return (flag & 15) == 0;
}

int
isEdgeBoundaryCell(int flag) {
  return !isFluidCell(flag) && !isInternalBoundaryCell(flag);
}

int
isPressureBoundary(int flag) {
  return (flag & C_P) == C_P;
}
