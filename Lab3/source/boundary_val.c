#include <stdio.h>
#include <string.h>

#include "scene.h"

/*----------------------------------------------------------------------------
 * East
 *--------------------------------------------------------------------------*/
void
setEastVelocityBoundary(int      i,
                        int      j,
                        double** U,
                        double** V) {
  U[i][j]     = 0;
  V[i][j]     = -V[i + 1][j];
  V[i][j - 1] = -V[i + 1][j - 1];
}

void
setEastPressureBoundary(int      i,
                        int      j,
                        double** P) {
  P[i][j] = P[i + 1][j];
}

void
setEastFgBoundary(int      i,
                  int      j,
                  double** U,
                  double** F) {
  F[i][j] = U[i][j];
}

/*----------------------------------------------------------------------------
 * West
 *--------------------------------------------------------------------------*/
void
setWestVelocityBoundary(int      i,
                        int      j,
                        double** U,
                        double** V) {
  U[i - 1][j] = 0;
  V[i][j]     = -V[i - 1][j];
  V[i][j - 1] = -V[i - 1][j - 1];
}

void
setWestPressureBoundary(int      i,
                        int      j,
                        double** P) {
  P[i][j] = P[i - 1][j];
}

void
setWestFgBoundary(int      i,
                  int      j,
                  double** U,
                  double** F) {
  F[i - 1][j] = U[i - 1][j];
}

/*----------------------------------------------------------------------------
 * South
 *--------------------------------------------------------------------------*/
void
setSouthVelocityBoundary(int      i,
                         int      j,
                         double** U,
                         double** V) {
  U[i][j]     = -U[i][j - 1];
  U[i - 1][j] = -U[i - 1][j - 1];
  V[i][j - 1] = 0;
}

void
setSouthPressureBoundary(int      i,
                         int      j,
                         double** P) {
  P[i][j] = P[i][j - 1];
}

void
setSouthFgBoundary(int      i,
                   int      j,
                   double** V,
                   double** G) {
  G[i][j - 1] = V[i][j - 1];
}

/*----------------------------------------------------------------------------
 * North
 *--------------------------------------------------------------------------*/
void
setNorthVelocityBoundary(int      i,
                         int      j,
                         double** U,
                         double** V) {
  U[i][j]     = -U[i][j + 1];
  U[i - 1][j] = -U[i - 1][j + 1];
  V[i][j]     = 0;
}

void
setNorthPressureBoundary(int      i,
                         int      j,
                         double** P) {
  P[i][j] = P[i][j + 1];
}

void
setNorthFgBoundary(int      i,
                   int      j,
                   double** V,
                   double** G) {
  G[i][j] = V[i][j];
}

/*----------------------------------------------------------------------------
 * South-East
 *--------------------------------------------------------------------------*/
void
setSouthEastVelocityBoundary(int      i,
                             int      j,
                             double** U,
                             double** V) {
  U[i][j]     = 0;
  V[i][j - 1] = 0;
  U[i - 1][j] = -U[i - 1][j - 1];
  V[i][j]     = -V[i + 1][j];
}

void
setSouthEastPressureBoundary(int      i,
                             int      j,
                             double** P) {
  P[i][j] = 0.5 * (P[i + 1][j] + P[i][j - 1]);
}

void
setSouthEastFgBoundary(int      i,
                       int      j,
                       double** U,
                       double** V,
                       double** F,
                       double** G) {
  F[i][j]     = U[i][j];
  G[i][j - 1] = V[i][j - 1];
}

/*----------------------------------------------------------------------------
 * South-West
 *--------------------------------------------------------------------------*/
void
setSouthWestVelocityBoundary(int      i,
                             int      j,
                             double** U,
                             double** V) {
  U[i - 1][j] = 0;
  V[i][j - 1] = 0;
  U[i][j]     = -U[i][j - 1];
  V[i][j]     = -V[i - 1][j];
}

void
setSouthWestPressureBoundary(int      i,
                             int      j,
                             double** P) {
  P[i][j] = 0.5 * (P[i - 1][j] + P[i][j - 1]);
}

void
setSouthWestFgBoundary(int      i,
                       int      j,
                       double** U,
                       double** V,
                       double** F,
                       double** G) {
  F[i - 1][j] = U[i - 1][j];
  G[i][j - 1] = V[i][j - 1];
}

/*----------------------------------------------------------------------------
 * North-East
 *--------------------------------------------------------------------------*/
void
setNorthEastVelocityBoundary(int      i,
                             int      j,
                             double** U,
                             double** V) {
  U[i][j]     = 0;
  V[i][j]     = 0;
  U[i - 1][j] = -U[i - 1][j + 1];
  V[i][j - 1] = -V[i + 1][j - 1];
}

void
setNorthEastPressureBoundary(int      i,
                             int      j,
                             double** P) {
  P[i][j] = 0.5 * (P[i + 1][j] + P[i][j + 1]);
}

void
setNorthEastFgBoundary(int      i,
                       int      j,
                       double** U,
                       double** V,
                       double** F,
                       double** G) {
  F[i][j] = U[i][j];
  G[i][j] = V[i][j];
}

/*----------------------------------------------------------------------------
 * North-West
 *--------------------------------------------------------------------------*/
void
setNorthWestVelocityBoundary(int      i,
                             int      j,
                             double** U,
                             double** V) {
  U[i - 1][j] = 0;
  V[i][j]     = 0;
  U[i][j]     = -U[i][j + 1];
  V[i][j - 1] = -V[i - 1][j - 1];
}

void
setNorthWestPressureBoundary(int      i,
                             int      j,
                             double** P) {
  P[i][j] = 0.5 * (P[i - 1][j] + P[i][j + 1]);
}

void
setNorthWestFgBoundary(int      i,
                       int      j,
                       double** U,
                       double** V,
                       double** F,
                       double** G) {
  F[i - 1][j] = U[i - 1][j];
  G[i][j]     = V[i][j];
}

void
setVelocityBoundary(int      imax,
                    int      jmax,
                    double** U,
                    double** V,
                    int**    Flag) {
  for (int j = 1; j <= jmax; ++j)
    for (int i = 1; i <= imax; ++i) {
      switch (Flag[i][j]) {
      case B_E:
        setEastVelocityBoundary(i, j, U, V);
        break;

      case B_W:
        setWestVelocityBoundary(i, j, U, V);
        break;

      case B_S:
        setSouthVelocityBoundary(i, j, U, V);
        break;

      case B_N:
        setNorthVelocityBoundary(i, j, U, V);
        break;

      case B_SE:
        setSouthEastVelocityBoundary(i, j, U, V);
        break;

      case B_SW:
        setSouthWestVelocityBoundary(i, j, U, V);
        break;

      case B_NE:
        setNorthEastVelocityBoundary(i, j, U, V);
        break;

      case B_NW:
        setNorthWestVelocityBoundary(i, j, U, V);
        break;

      default:

        if (isEdgeBoundaryCell(Flag[i][j]))
          printf("Forbidden Boundary Cell (Velocity, Flag[%i][%i]=%i)\n",
                 i,
                 j,
                 Flag[i][j]);
      }
    }

}

void
setFgBoundary(int      imax,
              int      jmax,
              double** U,
              double** V,
              double** F,
              double** G,
              int**    Flag) {
  for (int j = 1; j <= jmax; ++j)
    for (int i = 1; i <= imax; ++i) {
      switch (Flag[i][j]) {
      case B_E:
        setEastFgBoundary(i, j, U, F);
        break;

      case B_W:
        setWestFgBoundary(i, j, U, F);
        break;

      case B_S:
        setSouthFgBoundary(i, j, V, G);
        break;

      case B_N:
        setNorthFgBoundary(i, j, V, G);
        break;

      case B_SE:
        setSouthEastFgBoundary(i, j, U, V, F, G);
        break;

      case B_SW:
        setSouthWestFgBoundary(i, j, U, V, F, G);
        break;

      case B_NE:
        setNorthEastFgBoundary(i, j, U, V, F, G);
        break;

      case B_NW:
        setNorthWestFgBoundary(i, j, U, V, F, G);
        break;

      default:

        if (isEdgeBoundaryCell(Flag[i][j]))
          printf("Forbidden Boundary Cell (Fg, Flag[%i][%i]=%i)\n", i, j,
                 Flag[i][j]);
      }
    }

}

void
setPressureBoundary(int      imax,
                    int      jmax,
                    double   dP,
                    double** P,
                    int**    Flag) {
  for (int j = 1; j <= jmax; ++j)
    for (int i = 1; i <= imax; ++i) {
      switch (Flag[i][j]) {
      case B_E:
        setEastPressureBoundary(i, j, P);
        break;

      case B_W:
        setWestPressureBoundary(i, j, P);
        break;

      case B_S:
        setSouthPressureBoundary(i, j, P);
        break;

      case B_N:
        setNorthPressureBoundary(i, j, P);
        break;

      case B_SE:
        setSouthEastPressureBoundary(i, j, P);
        break;

      case B_SW:
        setSouthWestPressureBoundary(i, j, P);
        break;

      case B_NE:
        setNorthEastPressureBoundary(i, j, P);
        break;

      case B_NW:
        setNorthWestPressureBoundary(i, j, P);
        break;

      default:

        if (isEdgeBoundaryCell(Flag[i][j]))
          printf("Forbidden Boundary Cell (Pressure, Flag[%i][%i]=%i)\n", i, j,
                 Flag[i][j]);
      }
    }

  /* set boundary values */

  for (int i = 1; i <= imax; i++) {
    P[i][0]        = P[i][1];
    P[i][jmax + 1] = P[i][jmax];
  }

  for (int j = 1; j <= jmax; j++) {
    if (isPressureBoundary(Flag[0][j]))
      P[0][j] = 2.0 * dP - P[1][j];
    else
      P[0][j] = P[1][j];

    if (isPressureBoundary(Flag[imax + 1][j]))
      P[imax + 1][j] = 2.0 * dP - P[imax][j];
    else
      P[imax + 1][j] = P[imax][j];
  }
}

void
boundaryvalues(int      imax,
               int      jmax,
               int      wl,
               int      wr,
               int      wt,
               int      wb,
               double** U,
               double** V,
               int**    Flag) {
  setVelocityBoundary(imax,
                      jmax,
                      U,
                      V,
                      Flag);

  for (int i = 1; i <= imax; ++i) {
    switch (wt) {
    case 1:
      V[i][jmax]     = 0;
      U[i][jmax + 1] = -U[i][jmax];
      break;

    case 2:
      V[i][jmax]     = 0;
      U[i][jmax + 1] = U[i][jmax];
      break;

    case 3:
      V[i][jmax]     = V[i][jmax - 1];
      U[i][jmax + 1] = U[i][jmax];
      break;

    default:
      printf("Unknown value for the top boundary\n");
    }

    switch (wb) {
    case 1:
      V[i][0] = 0;
      U[i][0] = -U[i][1];
      break;

    case 2:
      V[i][0] = 0;
      U[i][0] = U[i][1];
      break;

    case 3:
      V[i][0] = V[i][1];
      U[i][0] = U[i][1];
      break;

    default:
      printf("Unknown value for the bottom boundary\n");
    }
  }

  for (int j = 1; j <= jmax; ++j) {
    switch (wl) {
    case 1:
      U[0][j] = 0;
      V[0][j] = -V[1][j];
      break;

    case 2:
      U[0][j] = 0;
      V[0][j] = V[1][j];
      break;

    case 3:
      U[0][j] = U[1][j];
      V[0][j] = V[1][j];
      break;

    default:
      printf("Unknown value for the left boundary\n");
    }

    switch (wr) {
    case 1:
      U[imax][j]     = 0;
      V[imax + 1][j] = -V[imax][j];
      break;

    case 2:
      U[imax][j]     = 0;
      V[imax + 1][j] = V[imax][j];
      break;

    case 3:
      U[imax][j]     = U[imax - 1][j];
      V[imax + 1][j] = V[imax][j];
      break;

    default:
      printf("Unknown value for the right boundary\n");
    }
  }
}

void
spec_boundary_val(char*    problem,
                  int      imax,
                  int      jmax,
                  double** U,
                  double** V) {
  if (strcmp(problem, THE_KARMAN_VORTEX_STREET) == 0)
    for (int j = 1; j <= jmax; ++j) {
      U[0][j] = THE_KARMAN_VORTEX_STREET_INFLOW_U;

      if (j != jmax)
        V[0][j] = THE_KARMAN_VORTEX_STREET_INFLOW_V;
    }

  else if (strcmp(problem, FLOW_OVER_STEP) == 0) {
    //  int oneHalf = jmax / 2;

    //  for (int j = 1; j <= jmax; ++j) {
    //    if (j <= oneHalf) {
    //      U[0][j] = 0.0;

    //      if (j != jmax)
    //        V[0][j] = 0.0;
    //    } else {
    //      U[0][j] = FLOW_OVER_STEP_INFLOW_U;

    //      if (j != jmax)
    //        V[0][j] = FLOW_OVER_STEP_INFLOW_V;
    //    }
    //  }
  }
}
