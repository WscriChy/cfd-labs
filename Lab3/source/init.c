#include <stdio.h>
#include <string.h>

#include "boundary_val.h"
#include "helper.h"
#include "init.h"
#include "math.h"
#include "scene.h"

int
read_parameters(const char* szFileName, /* name of the file */
                double*     Re,         /* reynolds number */
                double*     UI,         /* velocity x-direction */
                double*     VI,         /* velocity y-direction */
                double*     PI,         /* pressure */
                double*     GX,         /* gravitation x-direction */
                double*     GY,         /* gravitation y-direction */
                double*     t_end,      /* end time */
                double*     xlength,    /* length of the domain x-dir.*/
                double*     ylength,    /* length of the domain y-dir.*/
                double*     dt,         /* time step */
                double*     dx,         /* length of a cell x-dir. */
                double*     dy,         /* length of a cell y-dir. */
                int*        imax,       /* number of cells x-direction*/
                int*        jmax,       /* number of cells y-direction*/
                double*     alpha,      /* uppwind differencing factor*/
                double*     omg,        /* relaxation factor */
                double*     tau,        /* safety factor for time step*/
                int*        itermax,    /* max. number of iterations */
                double*     dP,         /* pressure difference */
                int*        wl,         /* left boundary condition */
                int*        wr,         /* right boundary condition */
                int*        wt,         /* top boundary condition */
                int*        wb,         /* bottom boundary condition */
                /* for pressure per time step */
                double*     eps,        /* accuracy bound for pressure*/
                double*     dt_value) { /* time for output */
  READ_DOUBLE(szFileName, *xlength);
  READ_DOUBLE(szFileName, *ylength);

  READ_DOUBLE(szFileName, *Re);
  READ_DOUBLE(szFileName, *t_end);
  READ_DOUBLE(szFileName, *dt);

  READ_INT(szFileName, *imax);
  READ_INT(szFileName, *jmax);

  READ_DOUBLE(szFileName, *omg);
  READ_DOUBLE(szFileName, *eps);
  READ_DOUBLE(szFileName, *tau);
  READ_DOUBLE(szFileName, *alpha);

  READ_INT(szFileName, *itermax);
  READ_DOUBLE(szFileName, *dt_value);

  READ_DOUBLE(szFileName, *UI);
  READ_DOUBLE(szFileName, *VI);
  READ_DOUBLE(szFileName, *GX);
  READ_DOUBLE(szFileName, *GY);
  READ_DOUBLE(szFileName, *PI);

  int   problemSize  = strlen(PLANE_SHEAR_FLOW);
  int   dataFileSize = problemSize + 5;
  char* dataFile     = malloc(sizeof(char) * dataFileSize);
  sprintf(dataFile, "%s.dat", PLANE_SHEAR_FLOW);

  if (strcmp(szFileName, dataFile) == 0)
    READ_DOUBLE(szFileName, *dP);

  free(dataFile);

  READ_INT(szFileName, *wl);
  READ_INT(szFileName, *wr);
  READ_INT(szFileName, *wt);
  READ_INT(szFileName, *wb);

  *dx = *xlength / (double)(*imax);
  *dy = *ylength / (double)(*jmax);

  return 1;
}

void
init_uvp(char*    problem,
         double   UI,
         double   VI,
         double   PI,
         int      imax,
         int      jmax,
         double   dP,
         double** U,
         double** V,
         double** P,
         int**    Flag) {
  for (int j = 0; j <= jmax + 1; ++j)
    for (int i = 0; i <= imax + 1; ++i) {
      if (isFluidCell(Flag[i][j])) {
        if (strcmp(problem, FLOW_OVER_STEP) == 0) {
          int oneHalf = jmax / 2;

          if (j <= oneHalf)
            U[i][j] = 0.0;
          else
            U[i][j] = UI;
        } else
          U[i][j] = UI;

        V[i][j] = VI;
        P[i][j] = PI;
      } else {
        U[i][j] = 0.0;
        V[i][j] = 0.0;
        P[i][j] = 0.0;
      }
    }

  setPressureBoundary(imax,
                      jmax,
                      dP,
                      P,
                      Flag);
}

void
init_flag(char* problem,
          int   imax,
          int   jmax,
          int** Flag) {
  for (int i = 0; i <= imax + 1; i++) {
    Flag[i][jmax + 1] = C_B;
    Flag[i][0]        = C_B;
  }

  for (int j = 0; j <= jmax + 1; j++) {
    Flag[imax + 1][j] = C_B;
    Flag[0][j]        = C_B;
  }

  if (strcmp(problem, THE_KARMAN_VORTEX_STREET) == 0) {
    printf("The Karman Vortex Street flag array initialization ...\n");
    int oneFifth = jmax / 5;
    int distance = 2 * oneFifth;

    for (int j = 1; j <= (jmax); ++j)
      for (int i = 1; i <= (imax); ++i) {
        if (i > distance && i <= (distance + oneFifth) &&
            j > distance && j <= (distance + oneFifth))
          if (absolute(i - j) < 2) {
            Flag[i][j] = C_B;
            continue;
          }

        Flag[i][j] = C_F;
      }

  } else if (strcmp(problem, PLANE_SHEAR_FLOW) == 0) {
    printf("Plane shear flow flag array initialization ...\n");

    for (int j = 1; j <= (jmax); ++j)
      for (int i = 1; i <= (imax); ++i)
        Flag[i][j] = C_F;



    for (int j = 1; j <= jmax; j++)
      Flag[imax + 1][j] |= C_P;
  } else if (strcmp(problem, FLOW_OVER_STEP) == 0) {
    printf("Flow over a step flag array initialization ...\n");
    int oneHalf = jmax / 2;

    for (int j = 1; j <= (jmax); ++j)
      for (int i = 1; i <= (imax); ++i) {
        if (i <= oneHalf &&
            j <= oneHalf)
          Flag[i][j] = C_B;
        else
          Flag[i][j] = C_F;
      }

  }

  for (int j = 1; j <= jmax; ++j)
    for (int i = 1; i <= imax; ++i)
      // Current cell is fluid
      if ((Flag[i][j] & C_F) == C_F) {
        Flag[i + 1][j] |= B_W;
        Flag[i][j + 1] |= B_S;
        Flag[i - 1][j] |= B_E;
        Flag[i][j - 1] |= B_N;
      }



  for (int j = 1; j <= jmax; ++j)
    for (int i = 1; i <= imax; ++i)
      if ((Flag[i][j] & C_F) != C_F)
        printf("Flag[%i][%i] = %i\n", i, j, Flag[i][j]);

}
