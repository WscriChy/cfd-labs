#include <math.h>
#include <stdio.h>

#include "boundary_val.h"
#include "scene.h"
#include "sor.h"

void
sor(
  double   omg,
  double   dx,
  double   dy,
  int      imax,
  int      jmax,
  double** P,
  double** RS,
  double   dP,
  int**    Flag,
  double*  res) {
  int i, j;

  double rloc;
  double coeff           = omg / (2.0 * (1.0 / (dx * dx) + 1.0 / (dy * dy)));
  double squareDXInverse = 1 / (dx * dx);
  double squareDYInverse = 1 / (dy * dy);

  /* SOR iteration */
  for (i = 1; i <= imax; i++)
    for (j = 1; j <= jmax; j++)
      if (isFluidCell(Flag[i][j]))
        P[i][j] = (1.0 - omg) * P[i][j] +
                  coeff * (squareDXInverse * (P[i + 1][j] + P[i - 1][j]) +
                           squareDYInverse * (P[i][j + 1] + P[i][j - 1]) -
                           RS[i][j]);

  /* compute the residual */
  rloc = 0;

  int count = 0;

  for (i = 1; i <= imax; i++)
    for (j = 1; j <= jmax; j++)
      if (isFluidCell(Flag[i][j])) {
        rloc +=
          (squareDXInverse * (P[i + 1][j] - 2.0 * P[i][j] + P[i - 1][j]) +
           squareDYInverse *
           (P[i][j + 1] - 2.0 * P[i][j] + P[i][j - 1]) - RS[i][j]) *
          (squareDXInverse *
           (P[i + 1][j] - 2.0 * P[i][j] + P[i - 1][j]) +
           squareDYInverse *
           (P[i][j + 1] - 2.0 * P[i][j] + P[i][j - 1]) - RS[i][j]);
        ++count;
      }

  rloc = rloc / count;
  rloc = sqrt(rloc);
  /* set residual */
  *res = rloc;

  setPressureBoundary(imax,
                      jmax,
                      dP,
                      P,
                      Flag);
}
