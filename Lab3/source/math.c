#include "math.h"

double
absolute(double value) {
  return value * ((value > 0) - (value < 0));
}

double
minimum(double value1,
        double value2,
        double value3) {
  if (value1 < value2) {
    if (value1 < value3)
      return value1;
    else
      return value3;
  } else {
    if (value2 < value3)
      return value2;
    else
      return value3;
  }
}

double
maximum(int      iMax,
        int      jMax,
        double** arrays) {
  double result = absolute(arrays[0][0]);

  for (int j = 0; j < jMax; ++j)
    for (int i = 0; i < iMax; ++i) {
      double absoluteValue = absolute(arrays[i][j]);

      if (result < absoluteValue)
        result = absoluteValue;
    }

  return result;
}
