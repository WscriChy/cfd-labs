#ifndef scene_h
#define scene_h

#define C_F 16
#define C_B 0
#define C_P 32

#define B_E 8
#define B_W 4
#define B_S 2
#define B_N 1

#define B_SE 10
#define B_SW 6
#define B_NE 9
#define B_NW 5

int isFluidCell(int flag);

int isFluidRightAdjacentCell(int flag);

int isFluidTopAdjacentCell(int flag);

int isEdgeBoundaryCell(int flag);

int isPressureBoundary(int flag);

#define THE_KARMAN_VORTEX_STREET          "1"
#define THE_KARMAN_VORTEX_STREET_INFLOW_U 1.0
#define THE_KARMAN_VORTEX_STREET_INFLOW_V 0.0

#define PLANE_SHEAR_FLOW "2"

#define FLOW_OVER_STEP          "3"
#define FLOW_OVER_STEP_INFLOW_U 1.0
#define FLOW_OVER_STEP_INFLOW_V 0.0

#endif /* ----- end scene_h ----- */
