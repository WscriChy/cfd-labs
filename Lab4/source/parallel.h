#ifndef PARALLEL_H
#define PARALLEL_H

void
initializeMPI(int* rank, int* numberOfRanks, int argc, char* argv[]);

void
finalizeMPI();

void
print(char const* text);

void
communicate(double*  collideField,
            double** sendBuffer,
            double** readBuffer,
            int      xlength,
            int      iProc,
            int      jProc,
            int      kProc,
            int      rank);

#endif
