#ifndef _COLLISION_H_
#define _COLLISION_H_

void computePostCollisionDistributions(double*             currentCell,
                                       const double* const tau,
                                       const double* const feq);

void doCollision(double*             collideField,
                 int*                flagField,
                 const double* const tau,
                 int                 xlength,
                 int                 iProc,
                 int                 jProc,
                 int                 kProc);

#endif
