#ifndef _INITLB_H_
#define _INITLB_H_

int
readParameters(int*    xlength,
               double* tau,
               double* wallVelocity,
               int*    iProc,
               int*    jProc,
               int*    kProc,
               int*    timesteps,
               int*    timestepsPerPlotting,
               int     argc,
               char*   argv[]);

void
initialiseFields(double* collideField,
                 double* streamField,
                 int*    flagField,
                 int     xlength,
                 int     iProc,
                 int     jProc,
                 int     kProc,
                 int     rank,
                 int     numberOfRanks);

void
initialiseBuffers(double** sendBuffer,
                  double** readBuffer,
                  int      xlength,
                  int      iProc,
                  int      jProc,
                  int      kProc);

#endif
