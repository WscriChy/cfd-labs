#include "boundary.h"

#include "LBDefinitions.h"
#include "computeCellValues.h"
#include "helper.h"

#include <assert.h>

int
isFluidCellIndex3(int* flagField,
                  int  x,
                  int  y,
                  int  z,
                  int  xlength,
                  int  iProc,
                  int  jProc,
                  int  kProc) {
  int flagIndex = FLAG_INDEX3(x, y, z, xlength, iProc, jProc, kProc);

  return x >= 0 && x <= xlength / iProc + 1 &&
         y >= 0 && y <= xlength / jProc + 1 &&
         z >= 0 && z <= xlength / kProc + 1 &&
         (flagField[flagIndex] == FLUID ||
          flagField[flagIndex] == PARALLEL_BOUNDARY);
}

void
treatBoundary(double*             collideField,
              int*                flagField,
              double const* const wallVelocity,
              int                 xlength,
              int                 iProc,
              int                 jProc,
              int                 kProc) {
  for (int x = 0; x < xlength / iProc + 2; ++x)
    for (int y = 0; y < xlength / jProc + 2; ++y)
      for (int z = 0; z < xlength / kProc + 2; ++z) {
        int flagIndex = FLAG_INDEX3(x, y, z, xlength, iProc, jProc, kProc);

        if (flagField[flagIndex] == FLUID ||
            flagField[flagIndex] == PARALLEL_BOUNDARY ||
            flagField[flagIndex] == NOTHING)
          continue;

        double* boundaryCell = collideField + CELL_INDEX3(x,
                                                          y,
                                                          z,
                                                          xlength,
                                                          iProc,
                                                          jProc,
                                                          kProc);

        for (int i = 0; i < Q; ++i) {
          int const* const c = LATTICEVELOCITIES[i];

          if (!isFluidCellIndex3(flagField,
                                 x + c[0],
                                 y + c[1],
                                 z + c[2],
                                 xlength,
                                 iProc,
                                 jProc,
                                 kProc))
            continue;

          double* fluidCell = collideField + CELL_INDEX3(x + c[0],
                                                         y + c[1],
                                                         z + c[2],
                                                         xlength,
                                                         iProc,
                                                         jProc,
                                                         kProc);

          switch (flagField[flagIndex]) {
          case NO_SLIP:
            boundaryCell[i] = fluidCell[Q - 1 - i];

            break;

          case MOVING_WALL: {
            double density;

            computeDensity(fluidCell, &density);

            boundaryCell[i] = fluidCell[Q - 1 - i] +
                              2 * LATTICEWEIGHTS[i] * (density) *
                              DOT3(c, wallVelocity) / C_S_2;

            break;
          }

          default:
            printf("This can never happen!");

            exit(1);
          }

          assert(boundaryCell[i] >= 0);
        }
      }

}
