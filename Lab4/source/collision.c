#include "collision.h"

#include "LBDefinitions.h"
#include "computeCellValues.h"

#include <assert.h>

void
computePostCollisionDistributions(double*             currentCell,
                                  double const* const tau,
                                  double const* const feq) {
  for (int i = 0; i < Q; ++i) {
    currentCell[i] -= (currentCell[i] - feq[i]) / *tau;

    assert(currentCell[i] >= 0);
  }
}

void
doCollision(double*             collideField,
            int*                flagField,
            double const* const tau,
            int                 xlength,
            int                 iProc,
            int                 jProc,
            int                 kProc) {
  for (int x = 0; x < xlength / iProc + 2; ++x)
    for (int y = 0; y < xlength / jProc + 2; ++y)
      for (int z = 0; z < xlength / kProc + 2; ++z) {
        if (flagField[FLAG_INDEX3(x,
                                  y,
                                  z,
                                  xlength,
                                  iProc,
                                  jProc,
                                  kProc)] != FLUID)
          continue;

        double* currentCell = collideField + CELL_INDEX3(x,
                                                         y,
                                                         z,
                                                         xlength,
                                                         iProc,
                                                         jProc,
                                                         kProc);

        double density;
        double velocity[3];
        double feq[Q];

        computeDensity(currentCell, &density);
        computeVelocity(currentCell, &density, velocity);
        computeFeq(&density, velocity, feq);
        computePostCollisionDistributions(currentCell, tau, feq);
      }

}
