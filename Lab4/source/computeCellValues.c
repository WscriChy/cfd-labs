#include "computeCellValues.h"

#include "LBDefinitions.h"

void
computeDensity(double const* const currentCell, double* density) {
  *density = currentCell[0];

  for (int i = 1; i < Q; ++i)
    *density += currentCell[i];
}

void
computeVelocity(double const* const currentCell,
                double const* const density,
                double*             velocity) {
  velocity[0] = currentCell[0] * LATTICEVELOCITIES[0][0];
  velocity[1] = currentCell[0] * LATTICEVELOCITIES[0][1];
  velocity[2] = currentCell[0] * LATTICEVELOCITIES[0][2];

  for (int i = 1; i < Q; ++i) {
    velocity[0] += currentCell[i] * LATTICEVELOCITIES[i][0];
    velocity[1] += currentCell[i] * LATTICEVELOCITIES[i][1];
    velocity[2] += currentCell[i] * LATTICEVELOCITIES[i][2];
  }

  velocity[0] /= *density;
  velocity[1] /= *density;
  velocity[2] /= *density;
}

void
computeFeq(double const* const density,
           double const* const velocity,
           double*             feq) {
  for (int i = 0; i < Q; ++i)
    feq[i] = LATTICEWEIGHTS[i] *
             *density *
             (1 +
              DOT3(LATTICEVELOCITIES[i], velocity) / C_S_2 +
              DOT3(LATTICEVELOCITIES[i], velocity) *
              DOT3(LATTICEVELOCITIES[i], velocity) / (2 * C_S_4) -
              DOT3(velocity, velocity) / (2 * C_S_2));
}
