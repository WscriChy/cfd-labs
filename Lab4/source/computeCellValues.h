#ifndef _COMPUTECELLVALUES_H_
#define _COMPUTECELLVALUES_H_

void computeDensity(const double* const currentCell, double* density);

void computeVelocity(const double* const currentCell,
                     const double* const density,
                     double*             velocity);

void computeFeq(const double* const density,
                const double* const velocity,
                double*             feq);

#endif
