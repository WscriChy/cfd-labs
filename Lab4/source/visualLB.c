#include "visualLB.h"

#include "LBDefinitions.h"
#include "computeCellValues.h"
#include "helper.h"

void
writeVtkHeader3(FILE* file);

void
writeVtkPoints3(FILE* file,
                int   xlength,
                int   iProc,
                int   jProc,
                int   kProc,
                int   rank);

void
writeVtkVelocity3(FILE*               file,
                  int                 xlength,
                  double const* const collideField,
                  int const* const    flagField,
                  int                 iProc,
                  int                 jProc,
                  int                 kProc);

void
writeVtkDensity3(FILE*               file,
                 int                 xlength,
                 double const* const collideField,
                 int const* const    flagField,
                 int                 iProc,
                 int                 jProc,
                 int                 kProc);

void
writeVtkOutput(double const* const collideField,
               int const* const    flagField,
               char const*         fileName,
               int                 t,
               int                 xlength,
               int                 iProc,
               int                 jProc,
               int                 kProc,
               int                 rank) {
  char timestepFileName[256];

  sprintf(timestepFileName, "%s.%i.%i.vtk", fileName, rank, t);

  FILE* file = fopen(timestepFileName, "w");

  if (file == 0) {
    char message[18 + 256];

    sprintf(message, "Failed to open '%s'.", timestepFileName);
    ERROR(message);

    exit(1);
  }

  writeVtkHeader3(file);
  writeVtkPoints3(file, xlength, iProc, jProc, kProc, rank);
  writeVtkVelocity3(file,
                    xlength,
                    collideField,
                    flagField,
                    iProc,
                    jProc,
                    kProc);
  writeVtkDensity3(file,
                   xlength,
                   collideField,
                   flagField,
                   iProc,
                   jProc,
                   kProc);

  if (fclose(file)) {
    char message[19 + 256];

    sprintf(message, "Failed to close '%s'.", timestepFileName);
    ERROR(message);

    exit(1);
  }
}

void
writeVtkHeader3(FILE* file) {
  fprintf(file, "# vtk DataFile Version 2.0\n");
  fprintf(file, "The Lattice-Boltzmann Method\n");
  fprintf(file, "ASCII\n");
  fprintf(file, "\n");
}

void
writeVtkPoints3(FILE* file,
                int   xlength,
                int   iProc,
                int   jProc,
                int   kProc,
                int   rank) {
  fprintf(file, "DATASET STRUCTURED_GRID\n");
  fprintf(file,
          "DIMENSIONS %i %i %i\n",
          xlength / iProc,
          xlength / jProc,
          xlength / kProc);
  fprintf(file, "POINTS %i double\n",
          (xlength / iProc) *
          (xlength / jProc) *
          (xlength / kProc));
  fprintf(file, "\n");

  int i = PROCESS_I(rank, iProc);
  int j = PROCESS_J(rank, iProc, jProc);
  int k = PROCESS_K(rank, iProc, jProc);

  double originX = i * (xlength / iProc);
  double originY = j * (xlength / jProc);
  double originZ = k * (xlength / kProc);

  for (int z = 0; z < xlength / kProc; ++z)
    for (int y = 0; y < xlength / jProc; ++y)
      for (int x = 0; x < xlength / iProc; ++x)
        fprintf(file, "%f %f %f\n", originX + x, originY + y, originZ + z);

  fprintf(file, "\n");
}

void
writeVtkVelocity3(FILE*               file,
                  int                 xlength,
                  double const* const collideField,
                  int const* const    flagField,
                  int                 iProc,
                  int                 jProc,
                  int                 kProc) {
  UNUSED(flagField);

  fprintf(file, "POINT_DATA %i\n",
          (xlength / iProc) *
          (xlength / jProc) *
          (xlength / kProc));
  fprintf(file, "VECTORS velocity double\n");
  fprintf(file, "\n");

  for (int z = 1; z < xlength / kProc + 1; ++z)
    for (int y = 1; y < xlength / jProc + 1; ++y)
      for (int x = 1; x < xlength / iProc + 1; ++x) {
        double const* currentCell = collideField +
                                    CELL_INDEX3(x,
                                                y,
                                                z,
                                                xlength,
                                                iProc,
                                                jProc,
                                                kProc);

        double density;
        double velocity[3];

        computeDensity(currentCell, &density);
        computeVelocity(currentCell, &density, velocity);

        fprintf(file, "%f %f %f\n", velocity[0], velocity[1], velocity[2]);
      }

  fprintf(file, "\n");
}

void
writeVtkDensity3(FILE*               file,
                 int                 xlength,
                 double const* const collideField,
                 int const* const    flagField,
                 int                 iProc,
                 int                 jProc,
                 int                 kProc) {
  UNUSED(flagField);

  fprintf(file, "CELL_DATA %i\n",
          (xlength / iProc - 1) *
          (xlength / jProc - 1) *
          (xlength / kProc - 1));
  fprintf(file, "SCALARS density double 1\n");
  fprintf(file, "LOOKUP_TABLE default\n");
  fprintf(file, "\n");

  for (int z = 1; z < xlength / kProc; ++z)
    for (int y = 1; y < xlength / jProc; ++y)
      for (int x = 1; x < xlength / iProc; ++x) {
        double const* currentCell = collideField +
                                    CELL_INDEX3(x,
                                                y,
                                                z,
                                                xlength,
                                                iProc,
                                                jProc,
                                                kProc);

        double density;

        computeDensity(currentCell, &density);

        fprintf(file, "%f\n", density);
      }

  fprintf(file, "\n");
}
