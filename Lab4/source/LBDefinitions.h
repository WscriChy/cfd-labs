#ifndef _LBDEFINITIONS_H_
#define _LBDEFINITIONS_H_

#include <math.h>

// That was sort of wierd decision to put `static` variables into the header
// file as it results in unnecessary duplication of all these variables across
// all the transaltion units which include this header file. What you really
// wanted to do was to declare these variables as `extern` in header file, and
// define them in some transaltion unit (perhaps `LBDefinitions.c`). To
// conclude, 1 header declares the variable as `extern`, 1 transaltion unit
// defines the variable (and can reference it too), all other translation units
// can only reference the variable.

#define Q 19

static int const LATTICEVELOCITIES[Q][3] = { { 0,  -1,  -1   },
                                             { -1, 0,   -1   },

                                             { 0,  0,   -1   },

                                             { 1,  0,   -1   },
                                             { 0,  1,   -1   },
                                             { -1, -1,  0    },

                                             { 0,  -1,  0    },

                                             { 1,  -1,  0    },

                                             { -1, 0,   0    },

                                             { 0,  0,   0    },

                                             { 1,  0,   0    },

                                             { -1, 1,   0    },

                                             { 0,  1,   0    },

                                             { 1,  1,   0    },
                                             { 0,  -1,  1    },
                                             { -1, 0,   1    },

                                             { 0,  0,   1    },

                                             { 1,  0,   1    },
                                             { 0,  1,   1    } };

static double const LATTICEWEIGHTS[Q] = { 1.0 / 36.0,
                                          1.0 / 36.0,

                                          2.0 / 36.0,

                                          1.0 / 36.0,
                                          1.0 / 36.0,
                                          1.0 / 36.0,

                                          2.0 / 36.0,

                                          1.0 / 36.0,

                                          2.0 / 36.0,

                                          12.0 / 36.0,

                                          2.0 / 36.0,

                                          1.0 / 36.0,

                                          2.0 / 36.0,

                                          1.0 / 36.0,
                                          1.0 / 36.0,
                                          1.0 / 36.0,

                                          2.0 / 36.0,

                                          1.0 / 36.0,
                                          1.0 / 36.0 };

static double const C_S =
  1.0 / 1.732050807568877193176604123436845839023590087890625;

#define FLUID             0
#define NO_SLIP           1
#define MOVING_WALL       2
#define PARALLEL_BOUNDARY 3
#define NOTHING           4

#define FLAG_FIELD_SIZE3(length, iProc, jProc, kProc) \
  ((length / iProc + 2) * (length / jProc + 2) * (length / kProc + 2))

#define CELL_FIELD_SIZE3(length, iProc, jProc, kProc) \
  (Q * FLAG_FIELD_SIZE3(length, iProc, jProc, kProc))

#define FLAG_INDEX3(x, y, z, length, iProc, jProc, kProc) \
  ((x) +                                                  \
   (y) * (length / iProc + 2) +                           \
   (z) * (length / iProc + 2) * (length / jProc + 2))

#define CELL_INDEX3(x, y, z, length, iProc, jProc, kProc) \
  (Q * FLAG_INDEX3(x, y, z, length, iProc, jProc, kProc))

#define DOT3(x, y) \
  (x[0] * y[0] + x[1] * y[1] + x[2] * y[2])

#define C_S_2 \
  (C_S * C_S)

#define C_S_4 \
  (C_S_2 * C_S_2)

#define BUFFER_LEFT   0
#define BUFFER_RIGHT  1
#define BUFFER_TOP    2
#define BUFFER_BOTTOM 3
#define BUFFER_FRONT  4
#define BUFFER_BACK   5

#define BUFFER_SIZE2(length, iProc, jProc) \
  (5 * (length / iProc + 2) * (length / jProc + 2))

#define BUFFER_CELL_INDEX2(x, y, length, iProc) \
  (5 * ((x) +                                   \
        (y) * (length / iProc + 2)))

#define PROCESS_I(rank, iProc) \
  ((rank) % (iProc))

#define PROCESS_J(rank, iProc, jProc) \
  (((rank) % ((iProc) * (jProc))) / iProc)

#define PROCESS_K(rank, iProc, jProc) \
  ((rank) / ((iProc) * (jProc)))

#define RANK(i, j, k, iProc, jProc, kProc) \
  ((i) + (j) * (iProc) + (k) * (iProc) * (jProc))

#endif
