#include "parallel.h"

#include "LBDefinitions.h"
#include "helper.h"

#include <mpi.h>

void
initializeMPI(int* rank, int* numberOfRanks, int argc, char* argv[]) {
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, rank);
  MPI_Comm_size(MPI_COMM_WORLD, numberOfRanks);
}

void
finalizeMPI() {
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
}

void
print(char const* text) {
  int rank;

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  fprintf(stdout, "Process #%d: %s\n", rank, text);
  fflush(stdout);
}

void
extractLeftToRight(double*  collideField,
                   double** sendBuffer,
                   int      xlength,
                   int      iProc,
                   int      jProc,
                   int      kProc,
                   int      rank) {
  int i = PROCESS_I(rank, iProc);

  if (i == iProc - 1)
    return;

  for (int y = 0; y < xlength / jProc + 2; ++y)
    for (int z = 0; z < xlength / kProc + 2; ++z) {
      double* collideCell = collideField + CELL_INDEX3(xlength / iProc,
                                                       y,
                                                       z,
                                                       xlength,
                                                       iProc,
                                                       jProc,
                                                       kProc);

      double* bufferCell = sendBuffer[BUFFER_RIGHT] +
                           BUFFER_CELL_INDEX2(y,
                                              z,
                                              xlength,
                                              jProc);

      for (int i = 0, j = 0; i < Q; ++i) {
        int const* const c = LATTICEVELOCITIES[i];

        if (c[0] > 0)
          bufferCell[j++] = collideCell[i];
      }
    }

}

void
extractRightToLeft(double*  collideField,
                   double** sendBuffer,
                   int      xlength,
                   int      iProc,
                   int      jProc,
                   int      kProc,
                   int      rank) {
  int i = PROCESS_I(rank, iProc);

  if (i == 0)
    return;

  for (int y = 0; y < xlength / jProc + 2; ++y)
    for (int z = 0; z < xlength / kProc + 2; ++z) {
      double* collideCell = collideField + CELL_INDEX3(1,
                                                       y,
                                                       z,
                                                       xlength,
                                                       iProc,
                                                       jProc,
                                                       kProc);

      double* bufferCell = sendBuffer[BUFFER_LEFT] +
                           BUFFER_CELL_INDEX2(y,
                                              z,
                                              xlength,
                                              jProc);

      for (int i = 0, j = 0; i < Q; ++i) {
        int const* const c = LATTICEVELOCITIES[i];

        if (c[0] < 0)
          bufferCell[j++] = collideCell[i];
      }
    }

}

void
extractBottomToTop(double*  collideField,
                   double** sendBuffer,
                   int      xlength,
                   int      iProc,
                   int      jProc,
                   int      kProc,
                   int      rank) {
  int j = PROCESS_J(rank, iProc, jProc);

  if (j == jProc - 1)
    return;

  for (int x = 0; x < xlength / iProc + 2; ++x)
    for (int z = 0; z < xlength / kProc + 2; ++z) {
      double* collideCell = collideField + CELL_INDEX3(x,
                                                       xlength / jProc,
                                                       z,
                                                       xlength,
                                                       iProc,
                                                       jProc,
                                                       kProc);

      double* bufferCell = sendBuffer[BUFFER_TOP] +
                           BUFFER_CELL_INDEX2(x,
                                              z,
                                              xlength,
                                              iProc);

      for (int i = 0, j = 0; i < Q; ++i) {
        int const* const c = LATTICEVELOCITIES[i];

        if (c[1] > 0)
          bufferCell[j++] = collideCell[i];
      }
    }

}

void
extractTopToBottom(double*  collideField,
                   double** sendBuffer,
                   int      xlength,
                   int      iProc,
                   int      jProc,
                   int      kProc,
                   int      rank) {
  int j = PROCESS_J(rank, iProc, jProc);

  if (j == 0)
    return;

  for (int x = 0; x < xlength / iProc + 2; ++x)
    for (int z = 0; z < xlength / kProc + 2; ++z) {
      double* collideCell = collideField + CELL_INDEX3(x,
                                                       1,
                                                       z,
                                                       xlength,
                                                       iProc,
                                                       jProc,
                                                       kProc);

      double* bufferCell = sendBuffer[BUFFER_BOTTOM] +
                           BUFFER_CELL_INDEX2(x,
                                              z,
                                              xlength,
                                              iProc);

      for (int i = 0, j = 0; i < Q; ++i) {
        int const* const c = LATTICEVELOCITIES[i];

        if (c[1] < 0)
          bufferCell[j++] = collideCell[i];
      }
    }

}

void
extractFrontToBack(double*  collideField,
                   double** sendBuffer,
                   int      xlength,
                   int      iProc,
                   int      jProc,
                   int      kProc,
                   int      rank) {
  int k = PROCESS_K(rank, iProc, jProc);

  if (k == kProc - 1)
    return;

  for (int x = 0; x < xlength / iProc + 2; ++x)
    for (int y = 0; y < xlength / jProc + 2; ++y) {
      double* collideCell = collideField + CELL_INDEX3(x,
                                                       y,
                                                       xlength / kProc,
                                                       xlength,
                                                       iProc,
                                                       jProc,
                                                       kProc);

      double* bufferCell = sendBuffer[BUFFER_BACK] +
                           BUFFER_CELL_INDEX2(x,
                                              y,
                                              xlength,
                                              iProc);

      for (int i = 0, j = 0; i < Q; ++i) {
        int const* const c = LATTICEVELOCITIES[i];

        if (c[2] > 0)
          bufferCell[j++] = collideCell[i];
      }
    }

}

void
extractBackToFront(double*  collideField,
                   double** sendBuffer,
                   int      xlength,
                   int      iProc,
                   int      jProc,
                   int      kProc,
                   int      rank) {
  UNUSED(kProc);

  int k = PROCESS_K(rank, iProc, jProc);

  if (k == 0)
    return;

  for (int x = 0; x < xlength / iProc + 2; ++x)
    for (int y = 0; y < xlength / jProc + 2; ++y) {
      double* collideCell = collideField + CELL_INDEX3(x,
                                                       y,
                                                       1,
                                                       xlength,
                                                       iProc,
                                                       jProc,
                                                       kProc);

      double* bufferCell = sendBuffer[BUFFER_FRONT] +
                           BUFFER_CELL_INDEX2(x,
                                              y,
                                              xlength,
                                              iProc);

      for (int i = 0, j = 0; i < Q; ++i) {
        int const* const c = LATTICEVELOCITIES[i];

        if (c[2] < 0)
          bufferCell[j++] = collideCell[i];
      }
    }

}

void
swapLeftToRight(double** sendBuffer,
                double** readBuffer,
                int      xlength,
                int      iProc,
                int      jProc,
                int      kProc,
                int      rank) {
  UNUSED(kProc);

  int i = PROCESS_I(rank, iProc);
  int j = PROCESS_J(rank, iProc, jProc);
  int k = PROCESS_K(rank, iProc, jProc);

  int leftRank;

  if (i == 0)
    leftRank = MPI_PROC_NULL;
  else
    leftRank = RANK(i - 1, j, k, iProc, jProc, kProc);

  int rightRank;

  if (i == iProc - 1)
    rightRank = MPI_PROC_NULL;
  else
    rightRank = RANK(i + 1, j, k, iProc, jProc, kProc);

  MPI_Status status;

  MPI_Sendrecv(sendBuffer[BUFFER_RIGHT],
               BUFFER_SIZE2(xlength, jProc, kProc),
               MPI_DOUBLE,
               rightRank,
               BUFFER_RIGHT,
               readBuffer[BUFFER_LEFT],
               BUFFER_SIZE2(xlength, jProc, kProc),
               MPI_DOUBLE,
               leftRank,
               BUFFER_RIGHT,
               MPI_COMM_WORLD,
               &status);
}

void
swapRightToLeft(double** sendBuffer,
                double** readBuffer,
                int      xlength,
                int      iProc,
                int      jProc,
                int      kProc,
                int      rank) {
  UNUSED(kProc);

  int i = PROCESS_I(rank, iProc);
  int j = PROCESS_J(rank, iProc, jProc);
  int k = PROCESS_K(rank, iProc, jProc);

  int leftRank;

  if (i == 0)
    leftRank = MPI_PROC_NULL;
  else
    leftRank = RANK(i - 1, j, k, iProc, jProc, kProc);

  int rightRank;

  if (i == iProc - 1)
    rightRank = MPI_PROC_NULL;
  else
    rightRank = RANK(i + 1, j, k, iProc, jProc, kProc);

  MPI_Status status;

  MPI_Sendrecv(sendBuffer[BUFFER_LEFT],
               BUFFER_SIZE2(xlength, jProc, kProc),
               MPI_DOUBLE,
               leftRank,
               BUFFER_LEFT,
               readBuffer[BUFFER_RIGHT],
               BUFFER_SIZE2(xlength, jProc, kProc),
               MPI_DOUBLE,
               rightRank,
               BUFFER_LEFT,
               MPI_COMM_WORLD,
               &status);
}

void
swapBottomToTop(double** sendBuffer,
                double** readBuffer,
                int      xlength,
                int      iProc,
                int      jProc,
                int      kProc,
                int      rank) {
  UNUSED(kProc);

  int i = PROCESS_I(rank, iProc);
  int j = PROCESS_J(rank, iProc, jProc);
  int k = PROCESS_K(rank, iProc, jProc);

  int bottomRank;

  if (j == 0)
    bottomRank = MPI_PROC_NULL;
  else
    bottomRank = RANK(i, j - 1, k, iProc, jProc, kProc);

  int topRank;

  if (j == jProc - 1)
    topRank = MPI_PROC_NULL;
  else
    topRank = RANK(i, j + 1, k, iProc, jProc, kProc);

  MPI_Status status;

  MPI_Sendrecv(sendBuffer[BUFFER_TOP],
               BUFFER_SIZE2(xlength, iProc, kProc),
               MPI_DOUBLE,
               topRank,
               BUFFER_TOP,
               readBuffer[BUFFER_BOTTOM],
               BUFFER_SIZE2(xlength, iProc, kProc),
               MPI_DOUBLE,
               bottomRank,
               BUFFER_TOP,
               MPI_COMM_WORLD,
               &status);
}

void
swapTopToBottom(double** sendBuffer,
                double** readBuffer,
                int      xlength,
                int      iProc,
                int      jProc,
                int      kProc,
                int      rank) {
  UNUSED(kProc);

  int i = PROCESS_I(rank, iProc);
  int j = PROCESS_J(rank, iProc, jProc);
  int k = PROCESS_K(rank, iProc, jProc);

  int bottomRank;

  if (j == 0)
    bottomRank = MPI_PROC_NULL;
  else
    bottomRank = RANK(i, j - 1, k, iProc, jProc, kProc);

  int topRank;

  if (j == jProc - 1)
    topRank = MPI_PROC_NULL;
  else
    topRank = RANK(i, j + 1, k, iProc, jProc, kProc);

  MPI_Status status;

  MPI_Sendrecv(sendBuffer[BUFFER_BOTTOM],
               BUFFER_SIZE2(xlength, iProc, kProc),
               MPI_DOUBLE,
               bottomRank,
               BUFFER_BOTTOM,
               readBuffer[BUFFER_TOP],
               BUFFER_SIZE2(xlength, iProc, kProc),
               MPI_DOUBLE,
               topRank,
               BUFFER_BOTTOM,
               MPI_COMM_WORLD,
               &status);
}

void
swapFrontToBack(double** sendBuffer,
                double** readBuffer,
                int      xlength,
                int      iProc,
                int      jProc,
                int      kProc,
                int      rank) {
  UNUSED(kProc);

  int i = PROCESS_I(rank, iProc);
  int j = PROCESS_J(rank, iProc, jProc);
  int k = PROCESS_K(rank, iProc, jProc);

  int frontRank;

  if (k == 0)
    frontRank = MPI_PROC_NULL;
  else
    frontRank = RANK(i, j, k - 1, iProc, jProc, kProc);

  int backRank;

  if (k == kProc - 1)
    backRank = MPI_PROC_NULL;
  else
    backRank = RANK(i, j, k + 1, iProc, jProc, kProc);

  MPI_Status status;

  MPI_Sendrecv(sendBuffer[BUFFER_BACK],
               BUFFER_SIZE2(xlength, iProc, jProc),
               MPI_DOUBLE,
               backRank,
               BUFFER_BACK,
               readBuffer[BUFFER_FRONT],
               BUFFER_SIZE2(xlength, iProc, jProc),
               MPI_DOUBLE,
               frontRank,
               BUFFER_BACK,
               MPI_COMM_WORLD,
               &status);
}

void
swapBackToFront(double** sendBuffer,
                double** readBuffer,
                int      xlength,
                int      iProc,
                int      jProc,
                int      kProc,
                int      rank) {
  UNUSED(kProc);

  int i = PROCESS_I(rank, iProc);
  int j = PROCESS_J(rank, iProc, jProc);
  int k = PROCESS_K(rank, iProc, jProc);

  int frontRank;

  if (k == 0)
    frontRank = MPI_PROC_NULL;
  else
    frontRank = RANK(i, j, k - 1, iProc, jProc, kProc);

  int backRank;

  if (k == kProc - 1)
    backRank = MPI_PROC_NULL;
  else
    backRank = RANK(i, j, k + 1, iProc, jProc, kProc);

  MPI_Status status;

  MPI_Sendrecv(sendBuffer[BUFFER_FRONT],
               BUFFER_SIZE2(xlength, iProc, jProc),
               MPI_DOUBLE,
               frontRank,
               BUFFER_FRONT,
               readBuffer[BUFFER_BACK],
               BUFFER_SIZE2(xlength, iProc, jProc),
               MPI_DOUBLE,
               backRank,
               BUFFER_FRONT,
               MPI_COMM_WORLD,
               &status);
}

void
injectLeftToRight(double*  collideField,
                  double** readBuffer,
                  int      xlength,
                  int      iProc,
                  int      jProc,
                  int      kProc,
                  int      rank) {
  int i = PROCESS_I(rank, iProc);

  if (i == 0)
    return;

  for (int y = 0; y < xlength / jProc + 2; ++y)
    for (int z = 0; z < xlength / kProc + 2; ++z) {
      double* collideCell = collideField + CELL_INDEX3(0,
                                                       y,
                                                       z,
                                                       xlength,
                                                       iProc,
                                                       jProc,
                                                       kProc);

      double* bufferCell = readBuffer[BUFFER_LEFT] +
                           BUFFER_CELL_INDEX2(y,
                                              z,
                                              xlength,
                                              jProc);

      for (int i = 0, j = 0; i < Q; ++i) {
        int const* const c = LATTICEVELOCITIES[i];

        if (c[0] > 0)
          collideCell[i] = bufferCell[j++];
      }
    }

}

void
injectRightToLeft(double*  collideField,
                  double** readBuffer,
                  int      xlength,
                  int      iProc,
                  int      jProc,
                  int      kProc,
                  int      rank) {
  int i = PROCESS_I(rank, iProc);

  if (i == iProc - 1)
    return;

  for (int y = 0; y < xlength / jProc + 2; ++y)
    for (int z = 0; z < xlength / kProc + 2; ++z) {
      double* collideCell = collideField + CELL_INDEX3(xlength / iProc + 1,
                                                       y,
                                                       z,
                                                       xlength,
                                                       iProc,
                                                       jProc,
                                                       kProc);

      double* bufferCell = readBuffer[BUFFER_RIGHT] +
                           BUFFER_CELL_INDEX2(y,
                                              z,
                                              xlength,
                                              jProc);

      for (int i = 0, j = 0; i < Q; ++i) {
        int const* const c = LATTICEVELOCITIES[i];

        if (c[0] < 0)
          collideCell[i] = bufferCell[j++];
      }
    }

}

void
injectBottomToTop(double*  collideField,
                  double** readBuffer,
                  int      xlength,
                  int      iProc,
                  int      jProc,
                  int      kProc,
                  int      rank) {
  int j = PROCESS_J(rank, iProc, jProc);

  if (j == 0)
    return;

  for (int x = 0; x < xlength / iProc + 2; ++x)
    for (int z = 0; z < xlength / kProc + 2; ++z) {
      double* collideCell = collideField + CELL_INDEX3(x,
                                                       0,
                                                       z,
                                                       xlength,
                                                       iProc,
                                                       jProc,
                                                       kProc);

      double* bufferCell = readBuffer[BUFFER_BOTTOM] +
                           BUFFER_CELL_INDEX2(x,
                                              z,
                                              xlength,
                                              iProc);

      for (int i = 0, j = 0; i < Q; ++i) {
        int const* const c = LATTICEVELOCITIES[i];

        if (c[1] > 0)
          collideCell[i] = bufferCell[j++];
      }
    }

}

void
injectTopToBottom(double*  collideField,
                  double** readBuffer,
                  int      xlength,
                  int      iProc,
                  int      jProc,
                  int      kProc,
                  int      rank) {
  int j = PROCESS_J(rank, iProc, jProc);

  if (j == jProc - 1)
    return;

  for (int x = 0; x < xlength / iProc + 2; ++x)
    for (int z = 0; z < xlength / kProc + 2; ++z) {
      double* collideCell = collideField + CELL_INDEX3(x,
                                                       xlength / jProc + 1,
                                                       z,
                                                       xlength,
                                                       iProc,
                                                       jProc,
                                                       kProc);

      double* bufferCell = readBuffer[BUFFER_TOP] +
                           BUFFER_CELL_INDEX2(x,
                                              z,
                                              xlength,
                                              iProc);

      for (int i = 0, j = 0; i < Q; ++i) {
        int const* const c = LATTICEVELOCITIES[i];

        if (c[1] < 0)
          collideCell[i] = bufferCell[j++];
      }
    }

}

void
injectFrontToBack(double*  collideField,
                  double** readBuffer,
                  int      xlength,
                  int      iProc,
                  int      jProc,
                  int      kProc,
                  int      rank) {
  UNUSED(kProc);

  int k = PROCESS_K(rank, iProc, jProc);

  if (k == 0)
    return;

  for (int x = 0; x < xlength / iProc + 2; ++x)
    for (int y = 0; y < xlength / jProc + 2; ++y) {
      double* collideCell = collideField + CELL_INDEX3(x,
                                                       y,
                                                       0,
                                                       xlength,
                                                       iProc,
                                                       jProc,
                                                       kProc);

      double* bufferCell = readBuffer[BUFFER_FRONT] +
                           BUFFER_CELL_INDEX2(x,
                                              y,
                                              xlength,
                                              iProc);

      for (int i = 0, j = 0; i < Q; ++i) {
        int const* const c = LATTICEVELOCITIES[i];

        if (c[2] > 0)
          collideCell[i] = bufferCell[j++];
      }
    }

}

void
injectBackToFront(double*  collideField,
                  double** readBuffer,
                  int      xlength,
                  int      iProc,
                  int      jProc,
                  int      kProc,
                  int      rank) {
  int k = PROCESS_K(rank, iProc, jProc);

  if (k == kProc - 1)
    return;

  for (int x = 0; x < xlength / iProc + 2; ++x)
    for (int y = 0; y < xlength / jProc + 2; ++y) {
      double* collideCell = collideField + CELL_INDEX3(x,
                                                       y,
                                                       xlength / kProc + 1,
                                                       xlength,
                                                       iProc,
                                                       jProc,
                                                       kProc);

      double* bufferCell = readBuffer[BUFFER_BACK] +
                           BUFFER_CELL_INDEX2(x,
                                              y,
                                              xlength,
                                              iProc);

      for (int i = 0, j = 0; i < Q; ++i) {
        int const* const c = LATTICEVELOCITIES[i];

        if (c[2] < 0)
          collideCell[i] = bufferCell[j++];
      }
    }

}

void
communicate(double*  collideField,
            double** sendBuffer,
            double** readBuffer,
            int      xlength,
            int      iProc,
            int      jProc,
            int      kProc,
            int      rank) {
  // Left to Right
  extractLeftToRight(collideField,
                     sendBuffer,
                     xlength,
                     iProc,
                     jProc,
                     kProc,
                     rank);
  swapLeftToRight(sendBuffer,
                  readBuffer,
                  xlength,
                  iProc,
                  jProc,
                  kProc,
                  rank);
  injectLeftToRight(collideField,
                    readBuffer,
                    xlength,
                    iProc,
                    jProc,
                    kProc,
                    rank);

  // Right to Left
  extractRightToLeft(collideField,
                     sendBuffer,
                     xlength,
                     iProc,
                     jProc,
                     kProc,
                     rank);
  swapRightToLeft(sendBuffer,
                  readBuffer,
                  xlength,
                  iProc,
                  jProc,
                  kProc,
                  rank);
  injectRightToLeft(collideField,
                    readBuffer,
                    xlength,
                    iProc,
                    jProc,
                    kProc,
                    rank);

  // Bottom to Top
  extractBottomToTop(collideField,
                     sendBuffer,
                     xlength,
                     iProc,
                     jProc,
                     kProc,
                     rank);
  swapBottomToTop(sendBuffer,
                  readBuffer,
                  xlength,
                  iProc,
                  jProc,
                  kProc,
                  rank);
  injectBottomToTop(collideField,
                    readBuffer,
                    xlength,
                    iProc,
                    jProc,
                    kProc,
                    rank);

  // Top to Bottom
  extractTopToBottom(collideField,
                     sendBuffer,
                     xlength,
                     iProc,
                     jProc,
                     kProc,
                     rank);
  swapTopToBottom(sendBuffer,
                  readBuffer,
                  xlength,
                  iProc,
                  jProc,
                  kProc,
                  rank);
  injectTopToBottom(collideField,
                    readBuffer,
                    xlength,
                    iProc,
                    jProc,
                    kProc,
                    rank);

  // Front to Back
  extractFrontToBack(collideField,
                     sendBuffer,
                     xlength,
                     iProc,
                     jProc,
                     kProc,
                     rank);
  swapFrontToBack(sendBuffer,
                  readBuffer,
                  xlength,
                  iProc,
                  jProc,
                  kProc,
                  rank);
  injectFrontToBack(collideField,
                    readBuffer,
                    xlength,
                    iProc,
                    jProc,
                    kProc,
                    rank);

  // Back to Front
  extractBackToFront(collideField,
                     sendBuffer,
                     xlength,
                     iProc,
                     jProc,
                     kProc,
                     rank);
  swapBackToFront(sendBuffer,
                  readBuffer,
                  xlength,
                  iProc,
                  jProc,
                  kProc,
                  rank);
  injectBackToFront(collideField,
                    readBuffer,
                    xlength,
                    iProc,
                    jProc,
                    kProc,
                    rank);
}
