#ifndef _BOUNDARY_H_
#define _BOUNDARY_H_

void treatBoundary(double*             collideField,
                   int*                flagField,
                   const double* const wallVelocity,
                   int                 xlength,
                   int                 iProc,
                   int                 jProc,
                   int                 kProc);

#endif
