#include "streaming.h"

#include "LBDefinitions.h"
#include "computeCellValues.h"

void
doStreaming(double* collideField,
            double* streamField,
            int*    flagField,
            int     xlength,
            int     iProc,
            int     jProc,
            int     kProc) {
  for (int x = 0; x < xlength / iProc + 2; ++x)
    for (int y = 0; y < xlength / jProc + 2; ++y)
      for (int z = 0; z < xlength / kProc + 2; ++z) {
        if (flagField[FLAG_INDEX3(x,
                                  y,
                                  z,
                                  xlength,
                                  iProc,
                                  jProc,
                                  kProc)] != FLUID)
          continue;

        double* streamCell = streamField + CELL_INDEX3(x,
                                                       y,
                                                       z,
                                                       xlength,
                                                       iProc,
                                                       jProc,
                                                       kProc);

        for (int i = 0; i < Q; ++i) {
          int const* const c = LATTICEVELOCITIES[i];

          double* collideCell = collideField + CELL_INDEX3(x - c[0],
                                                           y - c[1],
                                                           z - c[2],
                                                           xlength,
                                                           iProc,
                                                           jProc,
                                                           kProc);

          streamCell[i] = collideCell[i];
        }
      }

}
