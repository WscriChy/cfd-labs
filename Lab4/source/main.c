#include "LBDefinitions.h"
#include "boundary.h"
#include "collision.h"
#include "initLB.h"
#include "parallel.h"
#include "streaming.h"
#include "visualLB.h"

#include <assert.h>
#include <stdlib.h>

int
main(int argc, char* argv[]) {
  int    xlength;
  double tau;
  double wallVelocity[3];

  int iProc;
  int jProc;
  int kProc;

  int timesteps;
  int timestepsPerPlotting;

  int rank;
  int numberOfRanks;

  double* sendBuffer[6];
  double* readBuffer[6];

  char const* fileName = "output/Cavity";

  initializeMPI(&rank, &numberOfRanks, argc, argv);

  if (readParameters(&xlength,
                     &tau,
                     wallVelocity,
                     &iProc,
                     &jProc,
                     &kProc,
                     &timesteps,
                     &timestepsPerPlotting,
                     argc,
                     argv))
    return 1;

  assert(numberOfRanks == iProc * jProc * kProc);

  size_t cellFieldSize = CELL_FIELD_SIZE3(xlength, iProc, jProc, kProc);
  size_t flagFieldSize = FLAG_FIELD_SIZE3(xlength, iProc, jProc, kProc);

  double* collideField = (double*)malloc(cellFieldSize * sizeof(double));
  double* streamField  = (double*)malloc(cellFieldSize * sizeof(double));
  int*    flagField    = (int*)malloc(flagFieldSize * sizeof(int));

  initialiseFields(collideField,
                   streamField,
                   flagField,
                   xlength,
                   iProc,
                   jProc,
                   kProc,
                   rank,
                   numberOfRanks);

  initialiseBuffers(sendBuffer,
                    readBuffer,
                    xlength,
                    iProc,
                    jProc,
                    kProc);

  for (int t = 0; t < timesteps; ++t) {
    double* swap = 0;

    doStreaming(collideField,
                streamField,
                flagField,
                xlength,
                iProc,
                jProc,
                kProc);

    swap         = collideField;
    collideField = streamField;
    streamField  = swap;

    doCollision(collideField, flagField, &tau, xlength, iProc, jProc, kProc);

    // NOTE: In fact the algorithm proposed in the worksheet is not 100%
    // correct because it does communication only once, namely before streaming
    // and collision, what means that it treats boundary incorrectly. As the
    // time marching goes the solution still converges to the right one.
    // However, one can notice obviously incorrect velocities during the very
    // first time steps which are the result of incorrect boundary treament. To
    // amend this, one has to perform double communication: one after streaming
    // and collision and another one after boundary treatment (see below). The
    // reasoning is quiet wordy and requires some drawing to explain properly.
    // That's why we will introduce it during the defense. For now, please,
    // just bare with us... :)
    communicate(collideField,
                sendBuffer,
                readBuffer,
                xlength,
                iProc,
                jProc,
                kProc,
                rank);

    treatBoundary(collideField,
                  flagField,
                  wallVelocity,
                  xlength,
                  iProc,
                  jProc,
                  kProc);

    // NOTE: Furthermore, the during the 2nd communication (see below) even
    // less cells distributions have to be transmitted than during the 1st one
    // (see above). However, since it was not even mentioned in the worksheet,
    // we were too lazy to implement this special case to transmit the bare
    // minimum for the 2nd communication, and we rather used the same
    // `communicate` function (as for the 1st communication), which of course
    // does some redundant trasmission, but does the job anyway.
    communicate(collideField,
                sendBuffer,
                readBuffer,
                xlength,
                iProc,
                jProc,
                kProc,
                rank);

    if (t % timestepsPerPlotting == 0)
      writeVtkOutput(collideField,
                     flagField,
                     fileName,
                     t,
                     xlength,
                     iProc,
                     jProc,
                     kProc,
                     rank);
  }

  finalizeMPI();
}
