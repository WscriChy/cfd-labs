#ifndef _VISUALLB_H_
#define _VISUALLB_H_

void
writeVtkOutput(const double* const collideField,
               const int* const    flagField,
               const char*         filename,
               int                 t,
               int                 xlength,
               int                 iProc,
               int                 jProc,
               int                 kProc,
               int                 rank);

#endif
