#include "initLB.h"

#include "LBDefinitions.h"
#include "computeCellValues.h"
#include "helper.h"
#include "parallel.h"

int
readParameters(int*    xlength,
               double* tau,
               double* wallVelocity,
               int*    iProc,
               int*    jProc,
               int*    kProc,
               int*    timesteps,
               int*    timestepsPerPlotting,
               int     argc,
               char*   argv[]) {
  if (argc != 2)
    return 1;

  char const* fileName = argv[1];

  READ_INT(fileName, *xlength);

  READ_DOUBLE(fileName, *tau);

  read_double(fileName, "wallVelocityX", &wallVelocity[0]);
  read_double(fileName, "wallVelocityY", &wallVelocity[1]);
  read_double(fileName, "wallVelocityZ", &wallVelocity[2]);

  READ_INT(fileName, *iProc);
  READ_INT(fileName, *jProc);
  READ_INT(fileName, *kProc);

  READ_INT(fileName, *timesteps);
  READ_INT(fileName, *timestepsPerPlotting);

  return 0;
}

void
initialiseFields(double* collideField,
                 double* streamField,
                 int*    flagField,
                 int     xlength,
                 int     iProc,
                 int     jProc,
                 int     kProc,
                 int     rank,
                 int     numberOfRanks) {
  UNUSED(numberOfRanks);

#if 0
  double density     = 1;
  double velocity[3] = { 0 };
  double feq[Q];

  computeFeq(&density, velocity, feq);
#else
  double const* feq = LATTICEWEIGHTS;
#endif

  int i = PROCESS_I(rank, iProc);
  int j = PROCESS_J(rank, iProc, jProc);
  int k = PROCESS_K(rank, iProc, jProc);

  //////////////////
  char message[64];

  sprintf(message, "(%i, %i, %i)", i, j, k);

  print(message);
  //////////////////

  for (int x = 0; x < xlength / iProc + 2; ++x)
    for (int y = 0; y < xlength / jProc + 2; ++y)
      for (int z = 0; z < xlength / kProc + 2; ++z) {
        int cellIndex = CELL_INDEX3(x, y, z, xlength, iProc, jProc, kProc);

        memcpy(collideField + cellIndex, feq, Q * sizeof(double));
        memcpy(streamField  + cellIndex, feq, Q * sizeof(double));
      }



  for (int x = 1; x < xlength / iProc + 1; ++x)
    for (int y = 1; y < xlength / jProc + 1; ++y)
      for (int z = 1; z < xlength / kProc + 1; ++z)
        flagField[FLAG_INDEX3(x, y, z, xlength, iProc, jProc, kProc)] = FLUID;

  int flag;

  for (int x = 1; x < xlength / iProc + 1; ++x)
    for (int y = 1; y < xlength / jProc + 1; ++y) {
      if (k == 0)
        flag = NO_SLIP;
      else
        flag = PARALLEL_BOUNDARY;

      // Forward Wall
      flagField[FLAG_INDEX3(x,
                            y,
                            0,
                            xlength,
                            iProc,
                            jProc,
                            kProc)] = flag;

      if (k == kProc - 1)
        flag = NO_SLIP;
      else
        flag = PARALLEL_BOUNDARY;

      // Backward Wall
      flagField[FLAG_INDEX3(x,
                            y,
                            xlength / kProc + 1,
                            xlength,
                            iProc,
                            jProc,
                            kProc)] = flag;
    }



  for (int y = 1; y < xlength / jProc + 1; ++y)
    for (int z = 1; z < xlength / kProc + 1; ++z) {
      if (i == 0)
        flag = NO_SLIP;
      else
        flag = PARALLEL_BOUNDARY;

      // Left Wall
      flagField[FLAG_INDEX3(0,
                            y,
                            z,
                            xlength,
                            iProc,
                            jProc,
                            kProc)] = flag;

      if (i == iProc - 1)
        flag = NO_SLIP;
      else
        flag = PARALLEL_BOUNDARY;

      // Right Wall
      flagField[FLAG_INDEX3(xlength / iProc + 1,
                            y,
                            z,
                            xlength,
                            iProc,
                            jProc,
                            kProc)] = flag;
    }



  for (int x = 1; x < xlength / iProc + 1; ++x)
    for (int z = 1; z < xlength / kProc + 1; ++z) {
      if (j == 0)
        flag = NO_SLIP;
      else
        flag = PARALLEL_BOUNDARY;

      // Bottom Wall
      flagField[FLAG_INDEX3(x,
                            0,
                            z,
                            xlength,
                            iProc,
                            jProc,
                            kProc)] = flag;

      if (j == jProc - 1)
        flag = MOVING_WALL;
      else
        flag = PARALLEL_BOUNDARY;

      // Top Wall
      flagField[FLAG_INDEX3(x,
                            xlength / jProc + 1,
                            z,
                            xlength,
                            iProc,
                            jProc,
                            kProc)] = flag;
    }



  for (int x = 1; x < xlength / iProc + 1; ++x) {
    if (j == 0 && k == 0)
      flag = NO_SLIP;
    else if (j == 0 || k == 0)
      flag = NOTHING;
    else
      flag = PARALLEL_BOUNDARY;

    flagField[FLAG_INDEX3(x,
                          0,
                          0,
                          xlength,
                          iProc,
                          jProc,
                          kProc)] = flag;

    if (j == jProc - 1 && k == 0)
      flag = MOVING_WALL;
    else if (j == jProc - 1 || k == 0)
      flag = NOTHING;
    else
      flag = PARALLEL_BOUNDARY;

    flagField[FLAG_INDEX3(x,
                          xlength / jProc + 1,
                          0,
                          xlength,
                          iProc,
                          jProc,
                          kProc)] = flag;

    if (j == jProc - 1 && k == kProc - 1)
      flag = MOVING_WALL;
    else if (j == jProc - 1 || k == kProc - 1)
      flag = NOTHING;
    else
      flag = PARALLEL_BOUNDARY;

    flagField[FLAG_INDEX3(x,
                          xlength / jProc + 1,
                          xlength / kProc + 1,
                          xlength,
                          iProc,
                          jProc,
                          kProc)] = flag;

    if (j == 0 && k == kProc - 1)
      flag = NO_SLIP;
    else if (j == 0 || k == kProc - 1)
      flag = NOTHING;
    else
      flag = PARALLEL_BOUNDARY;

    flagField[FLAG_INDEX3(x,
                          0,
                          xlength / kProc + 1,
                          xlength,
                          iProc,
                          jProc,
                          kProc)] = flag;
  }

  for (int y = 1; y < xlength / jProc + 1; ++y) {
    if (i == 0 && k == 0)
      flag = NO_SLIP;
    else if (i == 0 || k == 0)
      flag = NOTHING;
    else
      flag = PARALLEL_BOUNDARY;

    flagField[FLAG_INDEX3(0,
                          y,
                          0,
                          xlength,
                          iProc,
                          jProc,
                          kProc)] = flag;

    if (i == iProc - 1 && k == 0)
      flag = NO_SLIP;
    else if (i == iProc - 1 || k == 0)
      flag = NOTHING;
    else
      flag = PARALLEL_BOUNDARY;

    flagField[FLAG_INDEX3(xlength / iProc + 1,
                          y,
                          0,
                          xlength,
                          iProc,
                          jProc,
                          kProc)] = flag;

    if (i == iProc - 1 && k == kProc - 1)
      flag = NO_SLIP;
    else if (i == iProc - 1 || k == kProc - 1)
      flag = NOTHING;
    else
      flag = PARALLEL_BOUNDARY;

    flagField[FLAG_INDEX3(xlength / iProc + 1,
                          y,
                          xlength / kProc + 1,
                          xlength,
                          iProc,
                          jProc,
                          kProc)] = flag;

    if (i == 0 && k == kProc - 1)
      flag = NO_SLIP;
    else if (i == 0 || k == kProc - 1)
      flag = NOTHING;
    else
      flag = PARALLEL_BOUNDARY;

    flagField[FLAG_INDEX3(0,
                          y,
                          xlength / kProc + 1,
                          xlength,
                          iProc,
                          jProc,
                          kProc)] = flag;
  }

  for (int z = 1; z < xlength / kProc + 1; ++z) {
    if (i == 0 && j == 0)
      flag = NO_SLIP;
    else if (i == 0 || j == 0)
      flag = NOTHING;
    else
      flag = PARALLEL_BOUNDARY;

    flagField[FLAG_INDEX3(0,
                          0,
                          z,
                          xlength,
                          iProc,
                          jProc,
                          kProc)] = flag;

    if (i == iProc - 1 && j == 0)
      flag = NO_SLIP;
    else if (i == iProc - 1 || j == 0)
      flag = NOTHING;
    else
      flag = PARALLEL_BOUNDARY;

    flagField[FLAG_INDEX3(xlength / iProc + 1,
                          0,
                          z,
                          xlength,
                          iProc,
                          jProc,
                          kProc)] = flag;

    if (i == iProc - 1 && j == jProc - 1)
      flag = MOVING_WALL;
    else if (i == iProc - 1 || j == jProc - 1)
      flag = NOTHING;
    else
      flag = PARALLEL_BOUNDARY;

    flagField[FLAG_INDEX3(xlength / iProc + 1,
                          xlength / jProc + 1,
                          z,
                          xlength,
                          iProc,
                          jProc,
                          kProc)] = flag;

    if (i == 0 && j == jProc - 1)
      flag = MOVING_WALL;
    else if (i == 0 || j == jProc - 1)
      flag = NOTHING;
    else
      flag = PARALLEL_BOUNDARY;

    flagField[FLAG_INDEX3(0,
                          xlength / jProc + 1,
                          z,
                          xlength,
                          iProc,
                          jProc,
                          kProc)] = flag;
  }

  if (i == 0 && j == 0 && k == 0)
    flag = NO_SLIP;
  else if (i == 0 || j == 0 || k == 0)
    flag = NOTHING;
  else
    flag = PARALLEL_BOUNDARY;

  flagField[FLAG_INDEX3(0,
                        0,
                        0,
                        xlength,
                        iProc,
                        jProc,
                        kProc)] = flag;

  if (i == iProc - 1 && j == 0 && k == 0)
    flag = NO_SLIP;
  else if (i == iProc - 1 || j == 0 || k == 0)
    flag = NOTHING;
  else
    flag = PARALLEL_BOUNDARY;

  flagField[FLAG_INDEX3(xlength / iProc + 1,
                        0,
                        0,
                        xlength,
                        iProc,
                        jProc,
                        kProc)] = flag;

  if (i == 0 && j == jProc - 1 && k == 0)
    flag = MOVING_WALL;
  else if (i == 0 || j == jProc - 1 || k == 0)
    flag = NOTHING;
  else
    flag = PARALLEL_BOUNDARY;

  flagField[FLAG_INDEX3(0,
                        xlength / jProc + 1,
                        0,
                        xlength,
                        iProc,
                        jProc,
                        kProc)] = flag;

  if (i == iProc - 1 && j == jProc - 1 && k == 0)
    flag = MOVING_WALL;
  else if (i == iProc - 1 || j == jProc - 1 || k == 0)
    flag = NOTHING;
  else
    flag = PARALLEL_BOUNDARY;

  flagField[FLAG_INDEX3(xlength / iProc + 1,
                        xlength / jProc + 1,
                        0,
                        xlength,
                        iProc,
                        jProc,
                        kProc)] = flag;

  if (i == 0 && j == 0 && k == kProc - 1)
    flag = NO_SLIP;
  else if (i == 0 || j == 0 || k == kProc - 1)
    flag = NOTHING;
  else
    flag = PARALLEL_BOUNDARY;

  flagField[FLAG_INDEX3(0,
                        0,
                        xlength / kProc + 1,
                        xlength,
                        iProc,
                        jProc,
                        kProc)] = flag;

  if (i == iProc - 1 && j == 0 && k == kProc - 1)
    flag = NO_SLIP;
  else if (i == iProc - 1 || j == 0 || k == kProc - 1)
    flag = NOTHING;
  else
    flag = PARALLEL_BOUNDARY;

  flagField[FLAG_INDEX3(xlength / iProc + 1,
                        0,
                        xlength / kProc + 1,
                        xlength,
                        iProc,
                        jProc,
                        kProc)] = flag;

  if (i == 0 && j == jProc - 1 && k == kProc - 1)
    flag = MOVING_WALL;
  else if (i == 0 || j == jProc - 1 || k == kProc - 1)
    flag = NOTHING;
  else
    flag = PARALLEL_BOUNDARY;

  flagField[FLAG_INDEX3(0,
                        xlength / jProc + 1,
                        xlength / kProc + 1,
                        xlength,
                        iProc,
                        jProc,
                        kProc)] = flag;

  if (i == iProc - 1 && j == jProc - 1 && k == kProc - 1)
    flag = MOVING_WALL;
  else if (i == iProc - 1 || j == jProc - 1 || k == kProc - 1)
    flag = NOTHING;
  else
    flag = PARALLEL_BOUNDARY;

  flagField[FLAG_INDEX3(xlength / iProc + 1,
                        xlength / jProc + 1,
                        xlength / kProc + 1,
                        xlength,
                        iProc,
                        jProc,
                        kProc)] = flag;
}

void
initialiseBuffers(double** sendBuffer,
                  double** readBuffer,
                  int      xlength,
                  int      iProc,
                  int      jProc,
                  int      kProc) {
  sendBuffer[BUFFER_LEFT] =
    (double*)malloc(BUFFER_SIZE2(xlength, jProc, kProc) * sizeof(double));

  readBuffer[BUFFER_LEFT] =
    (double*)malloc(BUFFER_SIZE2(xlength, jProc, kProc) * sizeof(double));

  sendBuffer[BUFFER_RIGHT] =
    (double*)malloc(BUFFER_SIZE2(xlength, jProc, kProc) * sizeof(double));

  readBuffer[BUFFER_RIGHT] =
    (double*)malloc(BUFFER_SIZE2(xlength, jProc, kProc) * sizeof(double));

  sendBuffer[BUFFER_TOP] =
    (double*)malloc(BUFFER_SIZE2(xlength, iProc, kProc) * sizeof(double));

  readBuffer[BUFFER_TOP] =
    (double*)malloc(BUFFER_SIZE2(xlength, iProc, kProc) * sizeof(double));

  sendBuffer[BUFFER_BOTTOM] =
    (double*)malloc(BUFFER_SIZE2(xlength, iProc, kProc) * sizeof(double));

  readBuffer[BUFFER_BOTTOM] =
    (double*)malloc(BUFFER_SIZE2(xlength, iProc, kProc) * sizeof(double));

  sendBuffer[BUFFER_FRONT] =
    (double*)malloc(BUFFER_SIZE2(xlength, iProc, jProc) * sizeof(double));

  readBuffer[BUFFER_FRONT] =
    (double*)malloc(BUFFER_SIZE2(xlength, iProc, jProc) * sizeof(double));

  sendBuffer[BUFFER_BACK] =
    (double*)malloc(BUFFER_SIZE2(xlength, iProc, jProc) * sizeof(double));

  readBuffer[BUFFER_BACK] =
    (double*)malloc(BUFFER_SIZE2(xlength, iProc, jProc) * sizeof(double));
}
