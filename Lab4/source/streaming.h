#ifndef _STREAMING_H_
#define _STREAMING_H_

void doStreaming(double* collideField,
                 double* streamField,
                 int*    flagField,
                 int     xlength,
                 int     iProc,
                 int     jProc,
                 int     kPro);

#endif
