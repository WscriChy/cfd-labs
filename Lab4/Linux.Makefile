BUILD_DIR   := build
INSTALL_DIR := install
CONFIG_DIR  := config
SOURCE_DIR  := source

CC     := mpicc
CFLAGS := -pipe -pedantic -Wall -Wextra -Werror -std=c99 \
          -O3 -fomit-frame-pointer -march=native

LDFLAGS := -lm

EXECUTABLE := lbsim

SOURCES := initLB.c \
           visualLB.c \
           boundary.c \
           collision.c \
           streaming.c \
           computeCellValues.c \
           main.c \
           parallel.c \
           helper.c

CONFIGS := Cavity1.cfg \
           Cavity2.cfg \
           Cavity3.cfg \
           Cavity4.cfg \
           Cavity5.cfg \
           Cavity6.cfg \
           Cavity7.cfg \
           Cavity8.cfg

# -----------------------------------------------------------------------------

OBJECTS := $(SOURCES:.c=.o)
OBJECTS := $(patsubst %,$(BUILD_DIR)/%,$(OBJECTS))
SOURCES := $(patsubst %,$(SOURCE_DIR)/%,$(SOURCES))
CONFIGS := $(patsubst %,$(CONFIG_DIR)/%,$(CONFIGS))

EXECUTABLE := $(patsubst %,$(BUILD_DIR)/%,$(EXECUTABLE))

.PHONY: all
all: $(EXECUTABLE)

.PHONY: clean
clean:
	@rm -f -r -d $(BUILD_DIR)/

.PHONY: install
install: install_executable install_configs

.PHONY: install_executable
install_executable: INSTALL_DIR $(EXECUTABLE)
	@cp $(EXECUTABLE) $(INSTALL_DIR)/

.PHONY: install_configs
install_configs: INSTALL_DIR $(CONFIGS)
	@cp $(CONFIGS) $(INSTALL_DIR)/

.PHONY: uninstall
uninstall:
	@rm -f -r -d $(INSTALL_DIR)/

$(EXECUTABLE): BUILD_DIR $(OBJECTS)
	$(CC) $(CFLAGS) -o $@ $(OBJECTS) $(LDFLAGS)

$(BUILD_DIR)/%.o: $(SOURCE_DIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@

BUILD_DIR:
	@mkdir -p $(BUILD_DIR)/

INSTALL_DIR:
	@mkdir -p $(INSTALL_DIR)/
	@mkdir -p $(INSTALL_DIR)/output/
